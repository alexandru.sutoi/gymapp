package com.example.fitnessapp.utils;

import com.example.fitnessapp.retrofit.getmealsuggestions.GetMealSuggestionsBody;
import com.example.fitnessapp.retrofit.getmealsuggestions.Plan;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class CustomMethods {
    public static GetMealSuggestionsBody getMealSuggestionsBody() {
        Map<String, String[]> planAcceptAllData = new LinkedHashMap<>();
        planAcceptAllData.put("health", new String[]{"SOY_FREE", "FISH_FREE", "MEDITERRANEAN"});

        List<Map<String, String[]>> planAcceptAll = new ArrayList<>();
        planAcceptAll.add(planAcceptAllData);

        Map<String, List<Map<String, String[]>>> planAcceptData = new LinkedHashMap<>();
        planAcceptData.put("all", planAcceptAll);

        Map<String, Integer> enercKcalData = new LinkedHashMap<>();
        enercKcalData.put("min", 1000);
        enercKcalData.put("max", 2000);

        Map<String, Integer> sugarAddedData = new LinkedHashMap<>();
        sugarAddedData.put("max", 20);

        Map<String, Map<String, Integer>> planFitData = new LinkedHashMap<>();
        planFitData.put("ENERC_KCAL", enercKcalData);
        planFitData.put("SUGAR.added", sugarAddedData);

        // Breakfast section
        Map<String, String[]> x = new LinkedHashMap<>();
        x.put("dish", new String[]{"drinks", "egg", "biscuits and cookies", "bread", "pancake", "cereals"});

        Map<String, String[]> y = new LinkedHashMap<>();
        y.put("meal", new String[]{"breakfast"});

        List<Map<String, String[]>> z = new ArrayList<>();
        z.add(x);
        z.add(y);

        Map<String, List<Map<String, String[]>>> a = new LinkedHashMap<>();
        a.put("all", z);

        Map<String, Integer> c = new LinkedHashMap<>();
        c.put("min", 100);
        c.put("max", 600);

        Map<String, Map<String, Integer>> d = new LinkedHashMap<>();
        d.put("ENERC_KCAL", c);

        Map<String, Object> f = new LinkedHashMap<>();
        f.put("accept", a);
        f.put("fit", d);
        Map<String, Object> planSectionsData = new LinkedHashMap<>();

        // Lunch section
        Map<String, String[]> a1 = new LinkedHashMap<>();
        a1.put("dish", new String[]{"main course", "pasta", "egg", "salad", "soup", "sandwiches", "pizza", "seafood"});

        Map<String, String[]> a2 = new LinkedHashMap<>();
        a2.put("meal", new String[]{"lunch/dinner"});

        List<Map<String, String[]>> a3 = new ArrayList<>();
        a3.add(a1);
        a3.add(a2);

        Map<String, List<Map<String, String[]>>> a4 = new LinkedHashMap<>();
        a4.put("all", a3);

        Map<String, Integer> a5 = new LinkedHashMap<>();
        a5.put("min", 300);
        a5.put("max", 900);

        Map<String, Map<String, Integer>> a6 = new LinkedHashMap<>();
        a6.put("ENERC_KCAL", a5);

        Map<String, Object> a7 = new LinkedHashMap<>();
        a7.put("accept", a4);
        a7.put("fit", a6);

        // Dinner section
        Map<String, String[]> b1 = new LinkedHashMap<>();
        b1.put("dish", new String[]{"seafood", "egg", "salad", "pizza", "pasta", "main course"});

        Map<String, String[]> b2 = new LinkedHashMap<>();
        b2.put("meal", new String[]{"lunch/dinner"});

        List<Map<String, String[]>> b3 = new ArrayList<>();
        b3.add(b1);
        b3.add(b2);

        Map<String, List<Map<String, String[]>>> b4 = new LinkedHashMap<>();
        b4.put("all", b3);

        Map<String, Integer> b5 = new LinkedHashMap<>();
        b5.put("min", 200);
        b5.put("max", 900);

        Map<String, Map<String, Integer>> b6 = new LinkedHashMap<>();
        b6.put("ENERC_KCAL", b5);

        Map<String, Object> b7 = new LinkedHashMap<>();
        b7.put("accept", b4);
        b7.put("fit", b6);

        //
        planSectionsData.put("Breakfast", f);
        planSectionsData.put("Lunch", a7);
        planSectionsData.put("Dinner", b7);

        return new GetMealSuggestionsBody(
                2,
                new Plan(
                        planAcceptData,
                        planFitData,
                        planSectionsData
                )
        );
    }
}
