package com.example.fitnessapp.enums;

public enum UserType {
    ADMIN,
    REGULAR
}
