package com.example.fitnessapp.enums;

public enum Gender {
    FEMALE,
    MALE
}
