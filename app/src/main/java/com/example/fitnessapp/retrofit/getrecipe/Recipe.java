package com.example.fitnessapp.retrofit.getrecipe;

import androidx.annotation.NonNull;

import java.util.Arrays;
import java.util.Map;

public class Recipe {
    private String uri;
    private String label;
    private String image;
    private String[] dietLabels;
    private String[] healthLabels;
    private String[] cautions;
    private String[] ingredientLines;
    private Ingredient[] ingredients;
    private Double calories;
    private Double totalTime;
    private String[] cuisineType;
    private String[] mealType;
    private String[] dishType;
    private Map<String, NutrientInfo> totalNutrients;
    private Map<String, NutrientInfo> totalDaily;
    private Digest[] digest;

    public Recipe(
            String uri,
            String label,
            String image,
            String[] dietLabels,
            String[] healthLabels,
            String[] cautions,
            String[] ingredientLines,
            Ingredient[] ingredients,
            Double calories,
            Double totalTime,
            String[] cuisineType,
            String[] mealType,
            String[] dishType,
            Map<String, NutrientInfo> totalNutrients,
            Map<String, NutrientInfo> totalDaily,
            Digest[] digest
    ) {
        this.uri = uri;
        this.label = label;
        this.image = image;
        this.dietLabels = dietLabels;
        this.healthLabels = healthLabels;
        this.cautions = cautions;
        this.ingredientLines = ingredientLines;
        this.ingredients = ingredients;
        this.calories = calories;
        this.totalTime = totalTime;
        this.cuisineType = cuisineType;
        this.mealType = mealType;
        this.dishType = dishType;
        this.totalNutrients = totalNutrients;
        this.totalDaily = totalDaily;
        this.digest = digest;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String[] getDietLabels() {
        return dietLabels;
    }

    public void setDietLabels(String[] dietLabels) {
        this.dietLabels = dietLabels;
    }

    public String[] getHealthLabels() {
        return healthLabels;
    }

    public void setHealthLabels(String[] healthLabels) {
        this.healthLabels = healthLabels;
    }

    public String[] getCautions() {
        return cautions;
    }

    public void setCautions(String[] cautions) {
        this.cautions = cautions;
    }

    public String[] getIngredientLines() {
        return ingredientLines;
    }

    public void setIngredientLines(String[] ingredientLines) {
        this.ingredientLines = ingredientLines;
    }

    public Ingredient[] getIngredients() {
        return ingredients;
    }

    public void setIngredients(Ingredient[] ingredients) {
        this.ingredients = ingredients;
    }

    public Double getCalories() {
        return calories;
    }

    public void setCalories(Double calories) {
        this.calories = calories;
    }

    public Double getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(Double totalTime) {
        this.totalTime = totalTime;
    }

    public String[] getCuisineType() {
        return cuisineType;
    }

    public void setCuisineType(String[] cuisineType) {
        this.cuisineType = cuisineType;
    }

    public String[] getMealType() {
        return mealType;
    }

    public void setMealType(String[] mealType) {
        this.mealType = mealType;
    }

    public String[] getDishType() {
        return dishType;
    }

    public void setDishType(String[] dishType) {
        this.dishType = dishType;
    }

    public Map<String, NutrientInfo> getTotalNutrients() {
        return totalNutrients;
    }

    public void setTotalNutrients(Map<String, NutrientInfo> totalNutrients) {
        this.totalNutrients = totalNutrients;
    }

    public Map<String, NutrientInfo> getTotalDaily() {
        return totalDaily;
    }

    public void setTotalDaily(Map<String, NutrientInfo> totalDaily) {
        this.totalDaily = totalDaily;
    }

    public Digest[] getDigest() {
        return digest;
    }

    public void setDigest(Digest[] digest) {
        this.digest = digest;
    }

    @NonNull
    @Override
    public String toString() {
        return "Recipe{" +
                "uri='" + uri + '\'' +
                ", label='" + label + '\'' +
                ", image='" + image + '\'' +
                ", dietLabels=" + Arrays.toString(dietLabels) +
                ", healthLabels=" + Arrays.toString(healthLabels) +
                ", cautions=" + Arrays.toString(cautions) +
                ", ingredientLines=" + Arrays.toString(ingredientLines) +
                ", ingredients=" + Arrays.toString(ingredients) +
                ", calories=" + calories +
                ", totalTime=" + totalTime +
                ", cuisineType=" + Arrays.toString(cuisineType) +
                ", mealType=" + Arrays.toString(mealType) +
                ", dishType=" + Arrays.toString(dishType) +
                ", totalNutrients=" + totalNutrients +
                ", totalDaily=" + totalDaily +
                ", digest=" + Arrays.toString(digest) +
                '}';
    }
}
