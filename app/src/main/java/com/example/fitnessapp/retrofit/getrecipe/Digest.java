package com.example.fitnessapp.retrofit.getrecipe;

import androidx.annotation.NonNull;

import java.util.Arrays;

public class Digest {
    private String label;
    private String tag;
    private String schemaOrgTag;
    private Double total;
    private boolean hasRDI;
    private Double daily;
    private String unit;
    private Digest[] sub;

    public Digest(
            String label,
            String tag,
            String schemaOrgTag,
            Double total,
            boolean hasRDI,
            Double daily,
            String unit,
            Digest[] sub
    ) {
        this.label = label;
        this.tag = tag;
        this.schemaOrgTag = schemaOrgTag;
        this.total = total;
        this.hasRDI = hasRDI;
        this.daily = daily;
        this.unit = unit;
        this.sub = sub;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getSchemaOrgTag() {
        return schemaOrgTag;
    }

    public void setSchemaOrgTag(String schemaOrgTag) {
        this.schemaOrgTag = schemaOrgTag;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public boolean isHasRDI() {
        return hasRDI;
    }

    public void setHasRDI(boolean hasRDI) {
        this.hasRDI = hasRDI;
    }

    public Double getDaily() {
        return daily;
    }

    public void setDaily(Double daily) {
        this.daily = daily;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Digest[] getSub() {
        return sub;
    }

    public void setSub(Digest[] sub) {
        this.sub = sub;
    }

    @NonNull
    @Override
    public String toString() {
        return "Digest{" +
                "label='" + label + '\'' +
                ", tag='" + tag + '\'' +
                ", schemaOrgTag='" + schemaOrgTag + '\'' +
                ", total=" + total +
                ", hasRDI=" + hasRDI +
                ", daily=" + daily +
                ", unit='" + unit + '\'' +
                ", sub=" + Arrays.toString(sub) +
                '}';
    }
}
