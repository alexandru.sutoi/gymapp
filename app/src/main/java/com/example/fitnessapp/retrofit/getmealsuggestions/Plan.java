package com.example.fitnessapp.retrofit.getmealsuggestions;

import androidx.annotation.NonNull;

import java.util.List;
import java.util.Map;

public class Plan {
    private Map<String, List<Map<String, String[]>>> accept;
    private Map<String, Map<String, Integer>> fit;
    private Map<String, Object> sections;

    public Plan(
            Map<String, List<Map<String, String[]>>> accept,
            Map<String, Map<String, Integer>> fit,
            Map<String, Object> sections
    ) {
        this.accept = accept;
        this.fit = fit;
        this.sections = sections;
    }

    public Map<String, List<Map<String, String[]>>> getAccept() {
        return accept;
    }

    public void setAccept(Map<String, List<Map<String, String[]>>> accept) {
        this.accept = accept;
    }

    public Map<String, Map<String, Integer>> getFit() {
        return fit;
    }

    public void setFit(Map<String, Map<String, Integer>> fit) {
        this.fit = fit;
    }

    public Map<String, Object> getSections() {
        return sections;
    }

    public void setSections(Map<String, Object> sections) {
        this.sections = sections;
    }

    @NonNull
    @Override
    public String toString() {
        return "Plan{" +
                "accept=" + accept +
                ", fit=" + fit +
                ", sections=" + sections +
                '}';
    }
}
