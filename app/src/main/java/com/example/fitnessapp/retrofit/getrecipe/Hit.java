package com.example.fitnessapp.retrofit.getrecipe;

import androidx.annotation.NonNull;

public class Hit {
    private Recipe recipe;

    public Hit(Recipe recipe) {
        this.recipe = recipe;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    @NonNull
    @Override
    public String toString() {
        return "Hit{" +
                "recipe=" + recipe +
                '}';
    }
}
