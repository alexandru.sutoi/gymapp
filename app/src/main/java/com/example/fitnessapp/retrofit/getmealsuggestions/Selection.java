package com.example.fitnessapp.retrofit.getmealsuggestions;

import androidx.annotation.NonNull;

import java.util.Map;

public class Selection {
    private Map<String, Map<String, String>> sections;

    public Selection(Map<String, Map<String, String>> sections) {
        this.sections = sections;
    }

    public Map<String, Map<String, String>> getSections() {
        return sections;
    }

    public void setSections(Map<String, Map<String, String>> sections) {
        this.sections = sections;
    }

    @NonNull
    @Override
    public String toString() {
        return "Selection{" +
                "sections=" + sections +
                '}';
    }
}
