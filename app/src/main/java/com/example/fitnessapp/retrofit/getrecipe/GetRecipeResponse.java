package com.example.fitnessapp.retrofit.getrecipe;

import androidx.annotation.NonNull;

import java.util.List;

public class GetRecipeResponse {
    private Integer from;
    private Integer to;
    private Integer count;
    private List<Hit> hits;

    public GetRecipeResponse(Integer from, Integer to, Integer count, List<Hit> hits) {
        this.from = from;
        this.to = to;
        this.count = count;
        this.hits = hits;
    }

    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    public Integer getTo() {
        return to;
    }

    public void setTo(Integer to) {
        this.to = to;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<Hit> getHits() {
        return hits;
    }

    public void setHits(List<Hit> hits) {
        this.hits = hits;
    }

    @NonNull
    @Override
    public String toString() {
        return "GetRecipeResponse{" +
                "from=" + from +
                ", to=" + to +
                ", count=" + count +
                ", hits=" + hits +
                '}';
    }
}
