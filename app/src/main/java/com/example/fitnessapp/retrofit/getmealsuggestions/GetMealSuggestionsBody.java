package com.example.fitnessapp.retrofit.getmealsuggestions;

import androidx.annotation.NonNull;

public class GetMealSuggestionsBody {
    private Integer size;
    private Plan plan;

    public GetMealSuggestionsBody(Integer size, Plan plan) {
        this.size = size;
        this.plan = plan;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Plan getPlan() {
        return plan;
    }

    public void setPlan(Plan plan) {
        this.plan = plan;
    }

    @NonNull
    @Override
    public String toString() {
        return "GetMealSuggestionsBody{" +
                "size=" + size +
                ", plan=" + plan +
                '}';
    }
}
