package com.example.fitnessapp.retrofit.getrecipe;

import androidx.annotation.NonNull;

public class Ingredient {
    private String text;
    private Double quantity;
    private String measure;
    private String food;
    private Double weight;
    private String foodCategory;
    private String foodId;
    private String image;

    public Ingredient(
            String text,
            Double quantity,
            String measure,
            String food,
            Double weight,
            String foodCategory,
            String foodId,
            String image
    ) {
        this.text = text;
        this.quantity = quantity;
        this.measure = measure;
        this.food = food;
        this.weight = weight;
        this.foodCategory = foodCategory;
        this.foodId = foodId;
        this.image = image;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public String getFood() {
        return food;
    }

    public void setFood(String food) {
        this.food = food;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getFoodCategory() {
        return foodCategory;
    }

    public void setFoodCategory(String foodCategory) {
        this.foodCategory = foodCategory;
    }

    public String getFoodId() {
        return foodId;
    }

    public void setFoodId(String foodId) {
        this.foodId = foodId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @NonNull
    @Override
    public String toString() {
        return "Ingredient{" +
                "text='" + text + '\'' +
                ", quantity=" + quantity +
                ", measure='" + measure + '\'' +
                ", food='" + food + '\'' +
                ", weight=" + weight +
                ", foodCategory='" + foodCategory + '\'' +
                ", foodId='" + foodId + '\'' +
                ", image='" + image + '\'' +
                '}';
    }
}
