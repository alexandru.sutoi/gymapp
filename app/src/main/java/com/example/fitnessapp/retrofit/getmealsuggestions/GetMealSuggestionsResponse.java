package com.example.fitnessapp.retrofit.getmealsuggestions;

import androidx.annotation.NonNull;

import java.util.List;

public class GetMealSuggestionsResponse {
    private String status;
    private List<Selection> selection;

    public GetMealSuggestionsResponse(String status, List<Selection> selection) {
        this.status = status;
        this.selection = selection;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Selection> getSelection() {
        return selection;
    }

    public void setSelection(List<Selection> selection) {
        this.selection = selection;
    }

    @NonNull
    @Override
    public String toString() {
        return "GetMealSuggestionsResponse{" +
                "status='" + status + '\'' +
                ", selection=" + selection +
                '}';
    }
}
