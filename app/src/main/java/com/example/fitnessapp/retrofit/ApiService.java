package com.example.fitnessapp.retrofit;

import com.example.fitnessapp.retrofit.getmealsuggestions.GetMealSuggestionsBody;
import com.example.fitnessapp.retrofit.getmealsuggestions.GetMealSuggestionsResponse;
import com.example.fitnessapp.retrofit.getrecipe.GetRecipeResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    @Headers({
            "accept: application/json",
            "Edamam-Account-User: golo1999",
            "Authorization: Basic ZDM2MzYxNGQ6ODBjODU1YzY3NDAyN2MyMzEyYTlhN2UzZjYzYWU5ZWI=",
            "Content-Type: application/json"
    })
    @POST("api/meal-planner/v1/{app_id}/select")
    Call<GetMealSuggestionsResponse> getMealSuggestions(
            @Path("app_id") String appId,
            @Body GetMealSuggestionsBody body
    );

    @Headers({
            "accept: application/json",
            "Edamam-Account-User: golo1999",
            "Accept-Language: en"
    })
    @GET("api/recipes/v2/by-uri")
    Call<GetRecipeResponse> getRecipe(
            @Query("type") String type,
            @Query("uri") String uri,
            @Query("app_id") String appId,
            @Query("app_key") String appKey
    );
}
