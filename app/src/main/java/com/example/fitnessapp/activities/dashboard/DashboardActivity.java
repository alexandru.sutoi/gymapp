package com.example.fitnessapp.activities.dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.example.fitnessapp.R;
import com.example.fitnessapp.activities.auth.LoginActivity;
import com.example.fitnessapp.activities.dashboard.adminonly.AddSubscriptionPlanActivity;
import com.example.fitnessapp.activities.dashboard.regularuseronly.SubscriptionsHistoryActivity;
import com.example.fitnessapp.enums.UserType;
import com.example.fitnessapp.models.GymEntry;
import com.example.fitnessapp.models.SubscriptionPlanDetails;
import com.example.fitnessapp.models.User;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class DashboardActivity extends AppCompatActivity {
    private CardView addSubscriptionPlanCard;
    private TextView athletesInTheGymTextView;
    private TextView averageTimeSpentTextView;
    private ImageView checkInCheckOutImageView;
    private CardView checkInCheckOutCard;
    private TextView checkInCheckOutTextView;
    private final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
    private final FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    private final FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
    private CardView gymScheduleCard;
    private Button logoutButton;
    private CoordinatorLayout mainLayout;
    private CardView nutritionCard;
    private TextView subscriptionExpirationDateTextView;
    private CardView subscriptionsHistoryCard;
    private CardView subscriptionPlansCard;
    private TextView subscriptionStatusTextView;
    private CardView topCard;
    private TextView topLayoutWelcomeTextView;
    private TextView totalCheckInsTextView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        setVariables();
        setOnClickListeners();
        hideUnavailableFeatures();
        setTopLayout();
        setTotalCheckIns();
        setAthletesInTheGym();
        setSubscriptionExpirationDate();
        setAverageTimeSpentInTheGym();
        setCheckInCheckOutTabStyle();
    }

    private void hideUnavailableFeatures() {
        databaseReference.child("users")
                .child(firebaseUser.getUid())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (!snapshot.exists()) {
                            return;
                        }

                        User userData = snapshot.getValue(User.class);

                        if (userData == null || userData.getType() == UserType.ADMIN) {
                            if (addSubscriptionPlanCard.getVisibility() == View.GONE) {
                                addSubscriptionPlanCard.setVisibility(View.VISIBLE);
                            }
                            if (checkInCheckOutCard.getVisibility() == View.VISIBLE) {
                                checkInCheckOutCard.setVisibility(View.GONE);

                            }
                            if (subscriptionsHistoryCard.getVisibility() == View.VISIBLE) {
                                subscriptionsHistoryCard.setVisibility(View.GONE);
                            }
                            return;
                        }

                        addSubscriptionPlanCard.setVisibility(View.GONE);
                        checkInCheckOutCard.setVisibility(View.VISIBLE);
                        subscriptionsHistoryCard.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
    }

    private void setAthletesInTheGym() {
        databaseReference.child("gymEntries").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (!snapshot.exists()) {
                    athletesInTheGymTextView.setText(getResources().getText(R.string.zero_digit));
                    return;
                }

                int athletesInTheGym = 0;
                for (DataSnapshot usersSnapshot : snapshot.getChildren()) {
                    for (DataSnapshot subscriptionsSnapshot : usersSnapshot.getChildren()) {
                        for (DataSnapshot gymEntriesSnapshot : subscriptionsSnapshot.getChildren()) {
                            GymEntry gymEntry = gymEntriesSnapshot.getValue(GymEntry.class);

                            if (gymEntry != null &&
                                    gymEntry.getEndDateTime() == null &&
                                    gymEntry.getStartDateTime() != null) {
                                ++athletesInTheGym;
                            }
                        }
                    }
                }

                athletesInTheGymTextView.setText(String.valueOf(athletesInTheGym));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void setAverageTimeSpentInTheGym() {
        databaseReference
                .child("gymEntries")
                .child(firebaseUser.getUid())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (!snapshot.exists()) {
                            averageTimeSpentTextView.setText(getResources().getText(R.string.n_a));
                            return;
                        }

                        int totalCheckIns = 0;
                        long totalTimeSpent = 0L;
                        for (DataSnapshot subscriptionsHistorySnapshot : snapshot.getChildren()) {
                            for (DataSnapshot myGymEntriesSnapshot : subscriptionsHistorySnapshot.getChildren()) {
                                GymEntry gymEntry = myGymEntriesSnapshot.getValue(GymEntry.class);

                                if (gymEntry != null &&
                                        gymEntry.getEndDateTime() != null &&
                                        gymEntry.getStartDateTime() != null) {
                                    ++totalCheckIns;

                                    Date gymEntryEndDate = new Date(Long.parseLong(gymEntry.getEndDateTime()));
                                    Date gymEntryStartDate = new Date(Long.parseLong(gymEntry.getStartDateTime()));
                                    long differenceInMilliseconds = (gymEntryEndDate.getTime() - gymEntryStartDate.getTime());

                                    totalTimeSpent += differenceInMilliseconds;
                                }
                            }
                        }

                        if (totalCheckIns == 0 && totalTimeSpent == 0L) {
                            averageTimeSpentTextView.setText(getResources().getText(R.string.n_a));
                        } else {
                            long averageTimeSpent = totalTimeSpent / totalCheckIns;
                            // Formatting the total time spent (hh:mm:ss)
                            long hours = TimeUnit.MILLISECONDS.toHours(averageTimeSpent);
                            long minutes = TimeUnit.MILLISECONDS.toMinutes(averageTimeSpent) % 60;
                            long seconds = TimeUnit.MILLISECONDS.toSeconds(averageTimeSpent) % 60;
                            String averageTimeSpentText =
                                    String.format(Locale.getDefault(),
                                            "%02dh:%02dm:%02ds", hours, minutes, seconds);
                            averageTimeSpentTextView.setText(averageTimeSpentText);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
    }

    private void setCheckInCheckOutTabStyle() {
        databaseReference.child("gymEntries")
                .child(firebaseUser.getUid())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (!snapshot.exists()) {
                            checkInCheckOutImageView.setImageResource(R.drawable.ic_login);
                            checkInCheckOutTextView.setText(getResources().getString(R.string.check_in));
                            return;
                        }

                        boolean isAlreadyCheckedIn = false;
                        for (DataSnapshot subscriptionsSnapshot : snapshot.getChildren()) {
                            for (DataSnapshot gymEntriesSnapshot : subscriptionsSnapshot.getChildren()) {
                                GymEntry gymEntry = gymEntriesSnapshot.getValue(GymEntry.class);

                                if (gymEntry != null &&
                                        gymEntry.getEndDateTime() == null &&
                                        gymEntry.getStartDateTime() != null) {
                                    isAlreadyCheckedIn = true;
                                    break;
                                }
                            }
                        }

                        int checkInCheckOutTabImageViewResource = isAlreadyCheckedIn ?
                                R.drawable.ic_logout : R.drawable.ic_login;
                        String checkInCheckOutTabText = isAlreadyCheckedIn ?
                                getResources().getString(R.string.check_out) :
                                getResources().getString(R.string.check_in);
                        checkInCheckOutImageView.setImageResource(checkInCheckOutTabImageViewResource);
                        checkInCheckOutTextView.setText(checkInCheckOutTabText);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
    }

    private void setOnClickListeners() {
        addSubscriptionPlanCard.setOnClickListener(v -> {
            Intent intent = new Intent(DashboardActivity.this, AddSubscriptionPlanActivity.class);
            startActivity(intent);
        });
        checkInCheckOutCard.setOnClickListener(v -> {
            if (firebaseUser == null || checkInCheckOutTextView.getText()
                    .equals(getResources().getString(R.string.loading_check_in_check_out_status))) {
                return;
            }

            databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if (!snapshot.hasChild("subscriptions") ||
                            !snapshot.child("subscriptions").hasChild(firebaseUser.getUid())) {
                        Snackbar.make(mainLayout, "You don't have an active subscription", Snackbar.LENGTH_SHORT)
                                .setBackgroundTint(getResources().getColor(R.color.silver, getTheme()))
                                .setTextColor(getResources().getColor(R.color.gunmetal, getTheme()))
                                .show();
                        return;
                    }

                    // Get current subscription's id
                    String currentGymSubscriptionId = null;
                    for (DataSnapshot subscriptionsSnapshot :
                            snapshot.child("subscriptions").child(firebaseUser.getUid()).getChildren()) {
                        SubscriptionPlanDetails details = subscriptionsSnapshot.getValue(SubscriptionPlanDetails.class);

                        if (details != null) {
                            Date subscriptionPlanEndDate =
                                    new Date(Long.parseLong(details.getEndDateTime()));

                            if (subscriptionPlanEndDate.after(new Date())) {
                                currentGymSubscriptionId = subscriptionsSnapshot.getKey();
                                break;
                            }
                        }
                    }

                    if (currentGymSubscriptionId == null) {
                        Snackbar.make(mainLayout, "You don't have an active subscription", Snackbar.LENGTH_SHORT)
                                .setBackgroundTint(getResources().getColor(R.color.silver, getTheme()))
                                .setTextColor(getResources().getColor(R.color.gunmetal, getTheme()))
                                .show();
                        return;
                    }

                    if (!snapshot.hasChild("gymSchedule")) {
                        Snackbar.make(mainLayout, "You can't use the gym when its schedule isn't set", Snackbar.LENGTH_SHORT)
                                .setBackgroundTint(getResources().getColor(R.color.silver, getTheme()))
                                .setTextColor(getResources().getColor(R.color.gunmetal, getTheme()))
                                .show();
                        return;
                    }

                    String[] dayNames = new DateFormatSymbols().getWeekdays();
                    Calendar date = Calendar.getInstance();
                    String todayName = dayNames[date.get(Calendar.DAY_OF_WEEK)].toLowerCase();

                    if (!snapshot.child("gymSchedule").hasChild(todayName)) {
                        Snackbar.make(mainLayout, "You can't use the gym today", Snackbar.LENGTH_SHORT)
                                .setBackgroundTint(getResources().getColor(R.color.silver, getTheme()))
                                .setTextColor(getResources().getColor(R.color.gunmetal, getTheme()))
                                .show();
                        return;
                    }

                    Object o = snapshot.child("gymSchedule").child(todayName).child("end").getValue();
                    boolean isGymOpen = o != null && date.get(Calendar.HOUR_OF_DAY) < Integer.parseInt(o.toString());

                    // Check in
                    if (!snapshot.exists() ||
                            !snapshot.hasChild("gymEntries") ||
                            !snapshot.child("gymEntries").hasChild(firebaseUser.getUid())) {
                        if (!isGymOpen) {
                            Snackbar.make(mainLayout, "The gym is currently closed", Snackbar.LENGTH_SHORT)
                                    .setBackgroundTint(getResources().getColor(R.color.silver, getTheme()))
                                    .setTextColor(getResources().getColor(R.color.gunmetal, getTheme()))
                                    .show();
                            return;
                        }

                        databaseReference.child("gymEntries")
                                .child(firebaseUser.getUid())
                                .child(currentGymSubscriptionId)
                                .push()
                                .setValue(new GymEntry(
                                        null,
                                        String.valueOf(new Date().getTime())
                                ));
                        return;
                    }

                    boolean isAlreadyCheckedIn = false;
                    String currentGymEntryId = null;
                    String currentGymEntryStartTime = null;
                    for (DataSnapshot subscriptionsHistorySnapshot : snapshot
                            .child("gymEntries")
                            .child(firebaseUser.getUid())
                            .getChildren()) {
                        for (DataSnapshot gymEntriesSnapshot : subscriptionsHistorySnapshot.getChildren()) {
                            GymEntry gymEntry = gymEntriesSnapshot.getValue(GymEntry.class);

                            if (gymEntry != null &&
                                    gymEntry.getEndDateTime() == null &&
                                    gymEntry.getStartDateTime() != null) {
                                isAlreadyCheckedIn = true;
                                currentGymEntryId = gymEntriesSnapshot.getKey();
                                currentGymEntryStartTime = gymEntry.getStartDateTime();
                                break;
                            }
                        }
                    }

                    // Check out
                    if (isAlreadyCheckedIn) {
                        if (currentGymEntryId != null && currentGymEntryStartTime != null) {
                            databaseReference.child("gymEntries")
                                    .child(firebaseUser.getUid())
                                    .child(currentGymSubscriptionId)
                                    .child(currentGymEntryId)
                                    .setValue(new GymEntry(
                                            String.valueOf(new Date().getTime()),
                                            currentGymEntryStartTime
                                    ));
                        }
                    }
                    // Check in
                    else {
                        if (!isGymOpen) {
                            Snackbar.make(mainLayout, "The gym is currently closed", Snackbar.LENGTH_SHORT)
                                    .setBackgroundTint(getResources().getColor(R.color.silver, getTheme()))
                                    .setTextColor(getResources().getColor(R.color.gunmetal, getTheme()))
                                    .show();
                            return;
                        }

                        databaseReference.child("gymEntries")
                                .child(firebaseUser.getUid())
                                .child(currentGymSubscriptionId)
                                .push()
                                .setValue(new GymEntry(
                                        null,
                                        String.valueOf(new Date().getTime())
                                ));
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        });
        gymScheduleCard.setOnClickListener(v -> {
            Intent intent = new Intent(DashboardActivity.this, GymScheduleActivity.class);
            startActivity(intent);
        });
        logoutButton.setOnClickListener(v -> {
            FirebaseAuth.getInstance().signOut();
            Intent intent = new Intent(DashboardActivity.this, LoginActivity.class);
            finishAffinity();
            startActivity(intent);
        });
        subscriptionsHistoryCard.setOnClickListener(v -> {
            Intent intent = new Intent(DashboardActivity.this, SubscriptionsHistoryActivity.class);
            startActivity(intent);
        });
        nutritionCard.setOnClickListener(v -> {
            Intent intent = new Intent(DashboardActivity.this, NutritionActivity.class);
            startActivity(intent);
        });
        subscriptionPlansCard.setOnClickListener(v -> {
            Intent intent = new Intent(DashboardActivity.this, SubscriptionPlansActivity.class);
            startActivity(intent);
        });
        topCard.setOnClickListener(v -> {
            Intent intent = new Intent(DashboardActivity.this, ProfileActivity.class);
            startActivity(intent);
        });
    }

    private void setSubscriptionExpirationDate() {
        databaseReference.child("subscriptions")
                .child(firebaseUser.getUid())
                .orderByChild("endDateTime")
                .limitToLast(1)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (!snapshot.exists()) {
                            subscriptionExpirationDateTextView.setText(getResources().getText(R.string.n_a));
                            return;
                        }

                        Date activeSubscriptionPlanExpirationDate = new Date();
                        boolean isSubscriptionActive = false;
                        for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                            SubscriptionPlanDetails subscriptionPlanDetails =
                                    dataSnapshot.getValue(SubscriptionPlanDetails.class);
                            if (subscriptionPlanDetails != null) {
                                Date subscriptionPlanEndDate =
                                        new Date(Long.parseLong(subscriptionPlanDetails.getEndDateTime()));

                                if (subscriptionPlanEndDate.after(new Date())) {
                                    activeSubscriptionPlanExpirationDate = subscriptionPlanEndDate;
                                    isSubscriptionActive = true;
                                }
                            }
                        }

                        if (!isSubscriptionActive) {
                            subscriptionExpirationDateTextView.setText(getResources().getText(R.string.n_a));
                        } else {
                            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
                            subscriptionExpirationDateTextView.setText(dateFormat.format(activeSubscriptionPlanExpirationDate));
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
    }

    private void setTopLayout() {
        String text = "Welcome, " + firebaseUser.getEmail() + "!";
        topLayoutWelcomeTextView.setText(text);

        // Ordering user's subscriptions by "endDateTime" field ascending and filtering only the last one
        // The snapshot contains only one item
        databaseReference.child("subscriptions")
                .child(firebaseUser.getUid())
                .orderByChild("endDateTime")
                .limitToLast(1)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (!snapshot.exists()) {
                            subscriptionStatusTextView.setText(getResources().getString(R.string.inactive_or_expired_subscription));
                            return;
                        }

                        boolean isSubscriptionActive = false;
                        for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                            SubscriptionPlanDetails subscriptionPlanDetails =
                                    dataSnapshot.getValue(SubscriptionPlanDetails.class);
                            if (subscriptionPlanDetails != null) {
                                Date subscriptionPlanEndDate =
                                        new Date(Long.parseLong(subscriptionPlanDetails.getEndDateTime()));

                                if (subscriptionPlanEndDate.after(new Date())) {
                                    isSubscriptionActive = true;
                                }
                            }
                        }

                        String subscriptionStatusText =
                                isSubscriptionActive ? "Active subscription" : "Inactive/expired subscription";
                        subscriptionStatusTextView.setText(subscriptionStatusText);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
    }

    private void setTotalCheckIns() {
        databaseReference
                .child("gymEntries")
                .child(firebaseUser.getUid())
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (!snapshot.exists()) {
                            totalCheckInsTextView.setText(getResources().getText(R.string.zero_digit));
                            return;
                        }

                        int totalCheckIns = 0;
                        for (DataSnapshot subscriptionsHistorySnapshot : snapshot.getChildren()) {
                            for (DataSnapshot myGymEntriesSnapshot : subscriptionsHistorySnapshot.getChildren()) {
                                GymEntry gymEntry = myGymEntriesSnapshot.getValue(GymEntry.class);

                                if (gymEntry != null &&
                                        gymEntry.getEndDateTime() != null &&
                                        gymEntry.getStartDateTime() != null) {
                                    ++totalCheckIns;
                                }
                            }
                        }

                        totalCheckInsTextView.setText(String.valueOf(totalCheckIns));
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
    }

    private void setVariables() {
        addSubscriptionPlanCard = findViewById(R.id.dashboardAddSubscriptionPlanCard);
        athletesInTheGymTextView = findViewById(R.id.dashboardAthletesInGymCountTextView);
        averageTimeSpentTextView = findViewById(R.id.dashboardAverageTimeSpentCountTextView);
        checkInCheckOutImageView = findViewById(R.id.dashboardCheckInCheckOutImageView);
        checkInCheckOutCard = findViewById(R.id.dashboardCheckInCheckOutCard);
        checkInCheckOutTextView = findViewById(R.id.dashboardCheckInCheckOutTextView);
        gymScheduleCard = findViewById(R.id.dashboardGymScheduleCard);
        logoutButton = findViewById(R.id.dashboardTopLayoutLogoutButton);
        mainLayout = findViewById(R.id.dashboardMainLayout);
        nutritionCard = findViewById(R.id.dashboardNutritionCard);
        subscriptionExpirationDateTextView = findViewById(R.id.dashboardSubscriptionExpirationDateTextView);
        subscriptionsHistoryCard = findViewById(R.id.subscriptionsHistoryCard);
        subscriptionPlansCard = findViewById(R.id.dashboardSubscriptionPlansCard);
        subscriptionStatusTextView = findViewById(R.id.dashboardTopLayoutSubscriptionStatusTextView);
        topCard = findViewById(R.id.dashboardTopCard);
        topLayoutWelcomeTextView = findViewById(R.id.dashboardTopLayoutWelcomeTextView);
        totalCheckInsTextView = findViewById(R.id.dashboardCheckInCountTextView);
    }
}