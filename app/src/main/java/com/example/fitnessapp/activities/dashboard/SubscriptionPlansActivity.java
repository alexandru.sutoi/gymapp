package com.example.fitnessapp.activities.dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.example.fitnessapp.R;
import com.example.fitnessapp.models.SubscriptionPlan;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class SubscriptionPlansActivity extends AppCompatActivity {
    private final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
    private LinearLayout mainLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription_plans);
        setVariables();
        setSubscriptionPlans();
    }

    private void setSubscriptionPlans() {
        databaseReference.child("subscriptionPlans")
                .orderByChild("price")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (!snapshot.exists()) {
                            return;
                        }

                        for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                            CardView cardLayout = (CardView) View.inflate(
                                    SubscriptionPlansActivity.this,
                                    R.layout.subscription_plan_card,
                                    null
                            );
                            LinearLayout.LayoutParams cardLayoutParams =
                                    new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                            LinearLayout.LayoutParams.WRAP_CONTENT);
                            TextView descriptionTextView =
                                    cardLayout.findViewById(R.id.subscriptionPlanDescription);
                            TextView nameTextView = cardLayout.findViewById(R.id.mealSuggestionName);
                            TextView priceTextView = cardLayout.findViewById(R.id.subscriptionPlanPrice);
                            SubscriptionPlan subscriptionPlan = dataSnapshot.getValue(SubscriptionPlan.class);

                            if (subscriptionPlan != null) {
                                subscriptionPlan.setId(dataSnapshot.getKey());
                                descriptionTextView.setText(subscriptionPlan.getDescription());
                                nameTextView.setText(subscriptionPlan.getName());
                                priceTextView.setText(String.valueOf(subscriptionPlan.getPrice()));

                                cardLayoutParams.setMargins(0, 32, 0, 0);

                                int cardLayoutId = View.generateViewId();
                                cardLayout.setId(cardLayoutId);
                                cardLayout.setLayoutParams(cardLayoutParams);
                                cardLayout.setOnClickListener(view -> {
                                    Intent intent = new Intent(
                                            SubscriptionPlansActivity.this,
                                            ActivateSubscriptionActivity.class
                                    );
                                    intent.putExtra("subscriptionPlan", subscriptionPlan);
                                    startActivity(intent);
                                });
                                mainLayout.addView(cardLayout);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
    }

    private void setVariables() {
        mainLayout = findViewById(R.id.subscriptionPlansMainLayout);
    }
}
