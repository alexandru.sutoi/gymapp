package com.example.fitnessapp.activities.dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.example.fitnessapp.R;
import com.example.fitnessapp.enums.Gender;
import com.example.fitnessapp.models.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

public class NutritionActivity extends AppCompatActivity {
    private final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
    private final FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    private LinearLayout cardsLayout;
    private Button seeMealSuggestionsButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nutrition);
        setVariables();
        setOnClickListeners();
        calculateBMR();
    }

    private void calculateBMR() {
        databaseReference.child("users")
                .child(firebaseUser.getUid())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (!snapshot.exists()) {
                            return;
                        }

                        User userData = snapshot.getValue(User.class);

                        if (userData == null) {
                            return;
                        }

                        Date birthDate = new Date(Long.parseLong(userData.getBirthDate()));
                        DateFormat formatter = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());
                        int birthDateInt = Integer.parseInt(formatter.format(birthDate));
                        int currentDateInt = Integer.parseInt(formatter.format(new Date()));
                        int age = (currentDateInt - birthDateInt) / 10000;
                        int lastAddition = userData.getGender() == Gender.FEMALE ? -161 : 5;
                        double bmr = 10 * userData.getWeight() + 6.25 * userData.getHeight() - 5 * age + lastAddition;
                        double activityFactorCoefficient;
                        switch (userData.getActivityFactor()) {
                            case LIGHTLY_ACTIVE:
                                activityFactorCoefficient = 1.375;
                                break;
                            case MODERATELY_ACTIVE:
                                activityFactorCoefficient = 1.55;
                                break;
                            case VERY_ACTIVE:
                                activityFactorCoefficient = 1.725;
                                break;
                            case EXTRA_ACTIVE:
                                activityFactorCoefficient = 1.9;
                                break;
                            default:
                                activityFactorCoefficient = 1.2;
                                break;
                        }

                        long dailyCaloricNeedsMaintain = Math.round(bmr * activityFactorCoefficient);

                        long dailyCaloricNeedsMildWeightLoss = dailyCaloricNeedsMaintain - 250;
                        long dailyCaloricNeedsWeightLoss = dailyCaloricNeedsMaintain - 500;
                        long dailyCaloricNeedsExtremeWeightLoss = dailyCaloricNeedsMaintain - 1000;

                        long dailyCaloricNeedsMildWeightGain = dailyCaloricNeedsMaintain + 250;
                        long dailyCaloricNeedsWeightGain = dailyCaloricNeedsMaintain + 500;
                        long dailyCaloricNeedsFastWeightGain = dailyCaloricNeedsMaintain + 1000;

                        Map<String, Long> dailyNeedsMap = new LinkedHashMap<>();
                        dailyNeedsMap.put("Maintain", dailyCaloricNeedsMaintain);
                        dailyNeedsMap.put("Mild weight loss", dailyCaloricNeedsMildWeightLoss);
                        dailyNeedsMap.put("Weight loss", dailyCaloricNeedsWeightLoss);
                        dailyNeedsMap.put("Extreme weight loss", dailyCaloricNeedsExtremeWeightLoss);
                        dailyNeedsMap.put("Mild weight gain", dailyCaloricNeedsMildWeightGain);
                        dailyNeedsMap.put("Weight gain", dailyCaloricNeedsWeightGain);
                        dailyNeedsMap.put("Fast weight gain", dailyCaloricNeedsFastWeightGain);

                        int cardIndex = -1;
                        for (Map.Entry<String, Long> entry : dailyNeedsMap.entrySet()) {
                            CardView nutritionCard = (CardView) View.inflate(
                                    NutritionActivity.this,
                                    R.layout.nutrition_card,
                                    null
                            );
                            LinearLayout.LayoutParams cardParams =
                                    new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                            LinearLayout.LayoutParams.WRAP_CONTENT);
                            TextView cardGoalName = nutritionCard.findViewById(R.id.nutritionCardGoalNameTextView);
                            TextView cardGoalValue = nutritionCard.findViewById(R.id.nutritionCardGoalValueTextView);

                            cardGoalName.setText(entry.getKey());
                            String cardGoalValueText = entry.getValue() + " " + "cal";
                            cardGoalValue.setText(cardGoalValueText);

                            ++cardIndex;
                            cardParams.setMargins(0, cardIndex == 0 ? 0 : 32, 0, 0);
                            nutritionCard.setLayoutParams(cardParams);
                            cardsLayout.addView(nutritionCard);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
    }

    private void setOnClickListeners() {
        seeMealSuggestionsButton.setOnClickListener(v -> {
            Intent intent = new Intent(NutritionActivity.this, MealSuggestionsActivity.class);
            startActivity(intent);
        });
    }

    private void setVariables() {
        cardsLayout = findViewById(R.id.nutritionCardsLayout);
        seeMealSuggestionsButton = findViewById(R.id.nutritionSeeMealSuggestionsButton);
    }
}
