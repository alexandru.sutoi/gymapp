package com.example.fitnessapp.activities.dashboard.regularuseronly;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.example.fitnessapp.R;
import com.example.fitnessapp.models.GymEntry;
import com.example.fitnessapp.models.SubscriptionPlan;
import com.example.fitnessapp.models.SubscriptionPlanDetails;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class SubscriptionsHistoryActivity extends AppCompatActivity {
    private final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
    private final FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    private LinearLayout mainLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscriptions_history);
        setVariables();
        setSubscriptions();
    }

    private void setSubscriptions() {
        FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();

        if (firebaseUser == null) {
            return;
        }

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (!snapshot.exists()) {
                    return;
                }

                if (!snapshot.hasChild("subscriptions") ||
                        !snapshot.child("subscriptions").hasChild(firebaseUser.getUid())) {
                    return;
                }

                // Adding snapshot children into a separate list to sort them in a descending ordering
                List<SubscriptionPlanDetails> subscriptionsHistory = new ArrayList<>();
                for (DataSnapshot subscriptionsHistorySnapshot :
                        snapshot.child("subscriptions").child(firebaseUser.getUid()).getChildren()) {
                    SubscriptionPlanDetails subscriptionPlanDetails =
                            subscriptionsHistorySnapshot.getValue(SubscriptionPlanDetails.class);
                    if (subscriptionPlanDetails != null) {
                        subscriptionPlanDetails.setId(subscriptionsHistorySnapshot.getKey());
                        subscriptionsHistory.add(subscriptionPlanDetails);
                    }
                }
                Collections.reverse(subscriptionsHistory);

                subscriptionsHistory.forEach(subscription -> {
                    CardView subscriptionCard = (CardView) View.inflate(
                            SubscriptionsHistoryActivity.this,
                            R.layout.subscriptions_history_card,
                            null
                    );
                    LinearLayout.LayoutParams subscriptionCardParams =
                            new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                    LinearLayout.LayoutParams.WRAP_CONTENT);
                    TextView cardName = subscriptionCard.findViewById(R.id.subscriptionsHistoryName);
                    TextView cardStatus = subscriptionCard.findViewById(R.id.subscriptionsHistoryStatus);
                    TextView cardTotalTimeSpent = subscriptionCard.findViewById(R.id.subscriptionsHistoryTotalTimeSpent);
                    TextView cardValidFrom = subscriptionCard.findViewById(R.id.subscriptionsHistoryValidFrom);
                    TextView cardValidUntil = subscriptionCard.findViewById(R.id.subscriptionsHistoryValidUntil);
                    TextView cardVisitsCount = subscriptionCard.findViewById(R.id.subscriptionsHistoryVisitsCount);

                    Date subscriptionEndDate = new Date(Long.parseLong(subscription.getEndDateTime()));
                    Date subscriptionStartDate = new Date(Long.parseLong(subscription.getStartDateTime()));
                    String nameText = subscription.getSubscriptionPlanId();
                    String statusText = subscriptionEndDate.after(new Date()) ? "Active" : "Expired";

                    // Retrieving my subscription's name from id;
                    if (snapshot.hasChild("subscriptionPlans") &&
                            snapshot.child("subscriptionPlans").hasChild(subscription.getSubscriptionPlanId())) {
                        SubscriptionPlan plan = snapshot.child("subscriptionPlans")
                                .child(subscription.getSubscriptionPlanId())
                                .getValue(SubscriptionPlan.class);

                        if (plan != null) {
                            nameText = plan.getName();
                        }
                    }

                    // Retrieving my subscription's visits count and total time spent from database
                    int gymVisitsCount = 0;
                    long totalTimeSpent = 0L;
                    if (snapshot.hasChild("gymEntries") &&
                            snapshot.child("gymEntries").hasChild(firebaseUser.getUid()) &&
                            snapshot.child("gymEntries")
                                    .child(firebaseUser.getUid())
                                    .hasChild(subscription.getId())) {
                        for (DataSnapshot myGymEntriesSnapshot :
                                snapshot.child("gymEntries")
                                        .child(firebaseUser.getUid())
                                        .child(subscription.getId())
                                        .getChildren()) {
                            GymEntry gymEntry = myGymEntriesSnapshot.getValue(GymEntry.class);

                            if (gymEntry != null &&
                                    gymEntry.getEndDateTime() != null &&
                                    gymEntry.getStartDateTime() != null) {
                                ++gymVisitsCount;
                                Date gymEntryEndDate = new Date(Long.parseLong(gymEntry.getEndDateTime()));
                                Date gymEntryStartDate = new Date(Long.parseLong(gymEntry.getStartDateTime()));
                                long differenceInMilliseconds = (gymEntryEndDate.getTime() - gymEntryStartDate.getTime());
                                totalTimeSpent += differenceInMilliseconds;
                            }
                        }
                    }

                    // Formatting the total time spent (hh:mm:ss)
                    long hours = TimeUnit.MILLISECONDS.toHours(totalTimeSpent);
                    long minutes = TimeUnit.MILLISECONDS.toMinutes(totalTimeSpent) % 60;
                    long seconds = TimeUnit.MILLISECONDS.toSeconds(totalTimeSpent) % 60;

                    cardName.setText(nameText);
                    cardStatus.setText(statusText);
                    cardTotalTimeSpent.setText(
                            String.format(
                                    Locale.getDefault(),
                                    "%02dh:%02dm:%02ds",
                                    hours,
                                    minutes,
                                    seconds
                            )
                    );
                    cardValidFrom.setText(subscriptionStartDate.toString());
                    cardValidUntil.setText(subscriptionEndDate.toString());
                    cardVisitsCount.setText(String.valueOf(gymVisitsCount));

                    subscriptionCardParams.setMargins(0, 32, 0, 0);
                    subscriptionCard.setLayoutParams(subscriptionCardParams);
                    mainLayout.addView(subscriptionCard);
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void setVariables() {
        mainLayout = findViewById(R.id.subscriptionsHistoryLayout);
    }
}
