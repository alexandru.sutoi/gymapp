package com.example.fitnessapp.activities.auth;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.fitnessapp.R;
import com.example.fitnessapp.activities.dashboard.DashboardActivity;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.regex.Pattern;

public class LoginActivity extends AppCompatActivity {
    private TextInputEditText emailAddressEditText;
    private TextInputLayout emailAddressEditTextLayout;
    private final FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    private TextView forgotPasswordTextView;
    private Button loginButton;
    private TextInputEditText passwordEditText;
    private TextInputLayout passwordEditTextLayout;
    private TextView registerTextView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initializeVariables();
        setOnClickListeners();
    }

    private void initializeVariables() {
        emailAddressEditText = findViewById(R.id.loginEmailAddressEditText);
        emailAddressEditTextLayout = findViewById(R.id.loginEmailAddressEditTextLayout);
        forgotPasswordTextView = findViewById(R.id.loginForgotPasswordTextView);
        loginButton = findViewById(R.id.loginButton);
        passwordEditText = findViewById(R.id.loginPasswordEditText);
        passwordEditTextLayout = findViewById(R.id.loginPasswordEditTextLayout);
        registerTextView = findViewById(R.id.loginRegisterTextView);
    }

    private void setOnClickListeners() {
        forgotPasswordTextView.setOnClickListener(v -> {
            Intent intent = new Intent(LoginActivity.this, ResetPasswordActivity.class);
            startActivity(intent);
        });
        loginButton.setOnClickListener(v -> {
            String emailAddress = String.valueOf(emailAddressEditText.getText()).trim();
            if (!Pattern.compile("^(.+)@(\\S+)$").matcher(emailAddress).matches()) {
                emailAddressEditTextLayout.setError("Email address is invalid.");
                return;
            } else if (emailAddressEditTextLayout.getError() != null) {
                emailAddressEditTextLayout.setError(null);
            }

            String password = String.valueOf(passwordEditText.getText());
            if (password.length() < 8) {
                passwordEditTextLayout.setError("Password must be at least 8 characters long.");
                return;
            } else if (passwordEditTextLayout.getError() != null) {
                passwordEditTextLayout.setError(null);
            }

            firebaseAuth.signInWithEmailAndPassword(emailAddress, password)
                    .addOnCompleteListener(this, signInTask -> {
                        if (signInTask.isSuccessful()) {
                            FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

                            if (firebaseUser == null) {
                                emailAddressEditTextLayout.setError("An error occurred. Please try again.");
                                return;
                            }

                            if (firebaseUser.isEmailVerified()) {
                                Intent intent = new Intent(this, DashboardActivity.class);
                                finishAffinity();
                                startActivity(intent);
                                Toast.makeText(LoginActivity.this, "Authentication successful.",
                                        Toast.LENGTH_SHORT).show();
                            } else {
                                firebaseUser.sendEmailVerification()
                                        .addOnCompleteListener(sendEmailVerificationTask -> {
                                            if (sendEmailVerificationTask.isSuccessful()) {
                                                emailAddressEditTextLayout.setError("Please verify your email address first.");
                                            } else if (sendEmailVerificationTask.getException() != null) {
                                                emailAddressEditTextLayout.setError(sendEmailVerificationTask
                                                        .getException()
                                                        .getLocalizedMessage());
                                            }
                                        })
                                        .addOnFailureListener(exception -> {
                                            String exceptionMessage = exception.getMessage();

                                            if (exceptionMessage != null) {
                                                emailAddressEditTextLayout.setError(exceptionMessage);
                                            }
                                        });
                            }
                        } else {
                            emailAddressEditTextLayout.setError(String.valueOf(signInTask.getException()));
                        }
                    }).addOnFailureListener(this, exception -> {
                                String exceptionMessage = exception.getMessage();

                                if (exceptionMessage != null) {
                                    emailAddressEditTextLayout.setError(exceptionMessage);
                                }
                            }
                    );
        });
        registerTextView.setOnClickListener(v -> {
            Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
            startActivity(intent);
        });
    }
}