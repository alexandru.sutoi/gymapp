package com.example.fitnessapp.activities.dashboard;

import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.fitnessapp.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class GymScheduleActivity extends AppCompatActivity {
    private DatabaseReference databaseReference;
    private TextView fridayValueTextView;
    private TextView mondayValueTextView;
    private TextView saturdayValueTextView;
    private TextView sundayValueTextView;
    private TextView thursdayValueTextView;
    private TextView tuesdayValueTextView;
    private TextView wednesdayValueTextView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gym_schedule);
        setVariables();
        setWorkingSchedule();
    }

    private void setVariables() {
        databaseReference = FirebaseDatabase.getInstance().getReference();
        fridayValueTextView = findViewById(R.id.gymScheduleFridayValueTextView);
        mondayValueTextView = findViewById(R.id.gymScheduleMondayValueTextView);
        saturdayValueTextView = findViewById(R.id.gymScheduleSaturdayValueTextView);
        sundayValueTextView = findViewById(R.id.gymScheduleSundayValueTextView);
        thursdayValueTextView = findViewById(R.id.gymScheduleThursdayValueTextView);
        tuesdayValueTextView = findViewById(R.id.gymScheduleTuesdayValueTextView);
        wednesdayValueTextView = findViewById(R.id.gymScheduleWednesdayValueTextView);
    }

    private void setWorkingSchedule() {
        databaseReference.child("gymSchedule").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (!snapshot.exists()) {
                    return;
                }

                if (snapshot.hasChild("monday") &&
                        snapshot.child("monday").hasChild("end") &&
                        snapshot.child("monday").hasChild("start")) {
                    String mondayValueText = snapshot.child("monday").child("start").getValue(Long.class)
                            + "-" +
                            snapshot.child("monday").child("end").getValue(Long.class);
                    mondayValueTextView.setText(mondayValueText);
                }

                if (snapshot.hasChild("tuesday") &&
                        snapshot.child("tuesday").hasChild("end") &&
                        snapshot.child("tuesday").hasChild("start")) {
                    String tuesdayValueText = snapshot.child("tuesday").child("start").getValue(Long.class)
                            + "-" +
                            snapshot.child("tuesday").child("end").getValue(Long.class);
                    tuesdayValueTextView.setText(tuesdayValueText);
                }

                if (snapshot.hasChild("wednesday") &&
                        snapshot.child("wednesday").hasChild("end") &&
                        snapshot.child("wednesday").hasChild("start")) {
                    String wednesdayValueText = snapshot.child("wednesday").child("start").getValue(Long.class)
                            + "-" +
                            snapshot.child("wednesday").child("end").getValue(Long.class);
                    wednesdayValueTextView.setText(wednesdayValueText);
                }

                if (snapshot.hasChild("thursday") &&
                        snapshot.child("thursday").hasChild("end") &&
                        snapshot.child("thursday").hasChild("start")) {
                    String thursdayValueText = snapshot.child("thursday").child("start").getValue(Long.class)
                            + "-" +
                            snapshot.child("thursday").child("end").getValue(Long.class);
                    thursdayValueTextView.setText(thursdayValueText);
                }

                if (snapshot.hasChild("friday") &&
                        snapshot.child("friday").hasChild("end") &&
                        snapshot.child("friday").hasChild("start")) {
                    String fridayValueText = snapshot.child("friday").child("start").getValue(Long.class)
                            + "-" +
                            snapshot.child("friday").child("end").getValue(Long.class);
                    fridayValueTextView.setText(fridayValueText);
                }

                if (snapshot.hasChild("saturday") &&
                        snapshot.child("saturday").hasChild("end") &&
                        snapshot.child("saturday").hasChild("start")) {
                    String saturdayValueText = snapshot.child("saturday").child("start").getValue(Long.class)
                            + "-" +
                            snapshot.child("saturday").child("end").getValue(Long.class);
                    saturdayValueTextView.setText(saturdayValueText);
                }

                if (snapshot.hasChild("sunday") &&
                        snapshot.child("sunday").hasChild("end") &&
                        snapshot.child("sunday").hasChild("start")) {
                    String sundayValueText = snapshot.child("sunday").child("start").getValue(Long.class)
                            + "-" +
                            snapshot.child("sunday").child("end").getValue(Long.class);
                    sundayValueTextView.setText(sundayValueText);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}
