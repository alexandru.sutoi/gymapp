package com.example.fitnessapp.activities.dashboard;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.example.fitnessapp.R;
import com.example.fitnessapp.retrofit.ApiService;
import com.example.fitnessapp.retrofit.getmealsuggestions.GetMealSuggestionsResponse;
import com.example.fitnessapp.retrofit.getrecipe.Digest;
import com.example.fitnessapp.retrofit.getrecipe.GetRecipeResponse;
import com.example.fitnessapp.retrofit.getrecipe.Recipe;
import com.example.fitnessapp.utils.CustomMethods;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MealSuggestionsActivity extends AppCompatActivity {
    private final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://api.edamam.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    private final ApiService apiService = retrofit.create(ApiService.class);

    private LinearLayout cardsLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meal_suggestions);
        setVariables();
        displaySuggestions();
    }

    private void displaySuggestions() {
        Call<GetMealSuggestionsResponse> getMealSuggestionsResponseCall = apiService.getMealSuggestions(
                "d363614d",
                CustomMethods.getMealSuggestionsBody()
        );
        getMealSuggestionsResponseCall.enqueue(new Callback<GetMealSuggestionsResponse>() {
            @Override
            public void onResponse(
                    @NonNull Call<GetMealSuggestionsResponse> call,
                    @NonNull Response<GetMealSuggestionsResponse> response
            ) {
                GetMealSuggestionsResponse getMealSuggestionsResponse = response.body();

                if (getMealSuggestionsResponse == null) {
                    return;
                }

                getMealSuggestionsResponse.getSelection().forEach(selection -> {
                    for (Map.Entry<String, Map<String, String>> selectionMapEntry :
                            selection.getSections().entrySet()) {
                        Map<String, String> y = selectionMapEntry.getValue();
                        for (Map.Entry<String, String> entry1 : y.entrySet()) {
                            Call<GetRecipeResponse> getRecipeResponseCall = apiService.getRecipe(
                                    "public",
                                    entry1.getValue(),
                                    "d363614d",
                                    "80c855c674027c2312a9a7e3f63ae9eb"
                            );
                            getRecipeResponseCall.enqueue(new Callback<GetRecipeResponse>() {
                                @Override
                                public void onResponse(
                                        @NonNull Call<GetRecipeResponse> call,
                                        @NonNull Response<GetRecipeResponse> response
                                ) {
                                    GetRecipeResponse recipeResponse = response.body();

                                    if (recipeResponse == null) {
                                        return;
                                    }

                                    recipeResponse.getHits().forEach(hit -> {
                                        CardView cardLayout = (CardView) View.inflate(
                                                MealSuggestionsActivity.this,
                                                R.layout.meal_suggestion_card,
                                                null
                                        );
                                        LinearLayout.LayoutParams cardLayoutParams =
                                                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                                        LinearLayout.LayoutParams.WRAP_CONTENT);
                                        TextView mealCaloriesTextView =
                                                cardLayout.findViewById(R.id.mealSuggestionCaloriesTextView);
                                        TextView mealCarbsTextView =
                                                cardLayout.findViewById(R.id.mealSuggestionCarbsTextView);
                                        TextView mealFatTextView =
                                                cardLayout.findViewById(R.id.mealSuggestionFatTextView);
                                        TextView mealProteinTextView =
                                                cardLayout.findViewById(R.id.mealSuggestionProteinTextView);
                                        TextView mealNameTextView = cardLayout.findViewById(R.id.mealSuggestionName);
                                        LinearLayout mealIngredientsLayout =
                                                cardLayout.findViewById(R.id.mealSuggestionIngredientsLayout);

                                        Recipe recipe = hit.getRecipe();

                                        for (String ingredient : recipe.getIngredientLines()) {
                                            TextView ingredientLineTextView = new TextView(MealSuggestionsActivity.this);
                                            ingredientLineTextView.setText(ingredient);
                                            ingredientLineTextView.setTextColor(getResources().getColor(R.color.silver, getTheme()));
                                            mealIngredientsLayout.addView(ingredientLineTextView);
                                        }

                                        for (Digest digest : recipe.getDigest()) {
                                            String digestText = Math.round(digest.getTotal()) + " " + digest.getUnit();
                                            if (digest.getLabel().equals("Carbs")) {
                                                mealCarbsTextView.setText(digestText);
                                            } else if (digest.getLabel().equals("Fat")) {
                                                mealFatTextView.setText(digestText);
                                            } else if (digest.getLabel().equals("Protein")) {
                                                mealProteinTextView.setText(digestText);
                                            }
                                        }

                                        Log.d("x", selectionMapEntry.getKey());
                                        Log.d("recipe", recipe.toString());
                                        Log.d("recipeLabel", recipe.getLabel());

                                        mealCaloriesTextView.setText(String.valueOf(Math.round(recipe.getCalories())));
                                        mealNameTextView.setText(recipe.getLabel());

                                        cardLayoutParams.setMargins(0, 32, 0, 0);
                                        cardLayout.setLayoutParams(cardLayoutParams);
                                        cardsLayout.addView(cardLayout);
                                    });
                                }

                                @Override
                                public void onFailure(@NonNull Call<GetRecipeResponse> call, @NonNull Throwable t) {

                                }
                            });
                        }
                    }
                });

                Log.d("mealSuggestions", getMealSuggestionsResponse.toString());
            }

            @Override
            public void onFailure(@NonNull Call<GetMealSuggestionsResponse> call, @NonNull Throwable t) {

            }
        });
    }

    private void setVariables() {
        cardsLayout = findViewById(R.id.mealSuggestionsCardsLayout);
    }
}
