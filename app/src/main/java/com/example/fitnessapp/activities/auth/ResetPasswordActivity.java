package com.example.fitnessapp.activities.auth;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.fitnessapp.R;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;

import java.util.regex.Pattern;

public class ResetPasswordActivity extends AppCompatActivity {
    private TextInputEditText emailAddressEditText;
    private TextInputLayout emailAddressEditTextLayout;
    private FirebaseAuth firebaseAuth;
    private TextView loginTextView;
    private Button resetPasswordButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        setVariables();
        setOnClickListeners();
    }

    private void setOnClickListeners() {
        loginTextView.setOnClickListener(v -> getOnBackPressedDispatcher().onBackPressed());
        resetPasswordButton.setOnClickListener(v -> {
            String emailAddress = String.valueOf(emailAddressEditText.getText()).trim();
            if (!Pattern.compile("^(.+)@(\\S+)$").matcher(emailAddress).matches()) {
                emailAddressEditTextLayout.setError("Email address is invalid.");
                return;
            } else if (emailAddressEditTextLayout.getError() != null) {
                emailAddressEditTextLayout.setError(null);
            }

            firebaseAuth.sendPasswordResetEmail(emailAddress)
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            Toast.makeText(
                                    ResetPasswordActivity.this,
                                    "Please verify your email address",
                                    Toast.LENGTH_SHORT
                            ).show();
                            getOnBackPressedDispatcher().onBackPressed();
                        } else if (task.getException() != null) {
                            emailAddressEditTextLayout.setError(task.getException().getLocalizedMessage());
                        }
                    }).addOnFailureListener(exception -> {
                        String exceptionMessage = exception.getMessage();

                        if (exceptionMessage != null) {
                            emailAddressEditTextLayout.setError(exceptionMessage);
                        }
                    });
        });
    }

    private void setVariables() {
        emailAddressEditText = findViewById(R.id.resetPasswordTextInputEditText);
        emailAddressEditTextLayout = findViewById(R.id.resetPasswordTextInputLayout);
        firebaseAuth = FirebaseAuth.getInstance();
        loginTextView = findViewById(R.id.resetPasswordLoginTextView);
        resetPasswordButton = findViewById(R.id.resetPasswordButton);
    }
}
