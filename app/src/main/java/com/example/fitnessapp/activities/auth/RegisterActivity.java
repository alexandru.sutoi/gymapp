package com.example.fitnessapp.activities.auth;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.fitnessapp.R;
import com.example.fitnessapp.enums.ActivityFactor;
import com.example.fitnessapp.enums.Gender;
import com.example.fitnessapp.enums.UserType;
import com.example.fitnessapp.models.User;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.radiobutton.MaterialRadioButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Pattern;

public class RegisterActivity extends AppCompatActivity {
    private AutoCompleteTextView activityFactorAutocompleteTextView;
    private MaterialDatePicker<Long> birthDatePicker;
    private TextInputEditText birthDateEditText;
    private final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
    private TextInputEditText emailAddressEditText;
    private TextInputLayout emailAddressEditTextLayout;
    private final FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    private TextInputEditText fullNameEditText;
    private TextInputLayout fullNameEditTextLayout;
    private RadioGroup genderRadioGroup;
    private TextInputEditText heightEditText;
    private TextInputLayout heightEditTextLayout;
    private TextView loginTextView;
    private TextInputEditText passwordEditText;
    private TextInputLayout passwordEditTextLayout;
    private TextInputEditText phoneNumberEditText;
    private TextInputLayout phoneNumberEditTextLayout;
    private long selectedBirthDateTimestamp = new Date().getTime();
    private MaterialButton submitButton;
    private RadioGroup userTypeRadioGroup;
    private TextInputEditText weightEditText;
    private TextInputLayout weightEditTextLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        setVariables();
        setOnClickListeners();
        setActivityFactorsDropdown();
        setBirthDateEditTextData();
    }

    private void setActivityFactorsDropdown() {
        String[] activityFactors = getResources().getStringArray(R.array.activity_factors);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, R.layout.activity_factor_item, activityFactors);

//        if (firebaseUser == null) {
//            activityFactorAutocompleteTextView.setText(activityFactors[0], false);
//            return;
//        }
//
//        databaseReference.child("users")
//                .child(firebaseUser.getUid())
//                .addListenerForSingleValueEvent(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(@NonNull DataSnapshot snapshot) {
//                        if (!snapshot.exists()) {
//                            activityFactorAutocompleteTextView.setText(activityFactors[0], false);
//                            return;
//                        }
//
//                        User userData = snapshot.getValue(User.class);
//
//                        if (userData == null || userData.getActivityFactor() == ActivityFactor.SEDENTARY) {
//                            activityFactorAutocompleteTextView.setText(activityFactors[0], false);
//                            return;
//                        }
//
//                        switch (userData.getActivityFactor()) {
//                            case EXTRA_ACTIVE:
//                                activityFactorAutocompleteTextView.setText(activityFactors[4], false);
//                                break;
//                            case LIGHTLY_ACTIVE:
//                                activityFactorAutocompleteTextView.setText(activityFactors[1], false);
//                                break;
//                            case MODERATELY_ACTIVE:
//                                activityFactorAutocompleteTextView.setText(activityFactors[2], false);
//                                break;
//                            case SEDENTARY:
//                                activityFactorAutocompleteTextView.setText(activityFactors[0], false);
//                                break;
//                            case VERY_ACTIVE:
//                                activityFactorAutocompleteTextView.setText(activityFactors[3], false);
//                                break;
//                        }
//                    }
//
//                    @Override
//                    public void onCancelled(@NonNull DatabaseError error) {
//
//                    }
//                });

        activityFactorAutocompleteTextView.setText(activityFactors[0], false);
        activityFactorAutocompleteTextView.setAdapter(arrayAdapter);
    }

    private void setBirthDateEditTextData() {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        birthDateEditText.setText(dateFormat.format(new Date(selectedBirthDateTimestamp)));
    }

    private void setOnClickListeners() {
        birthDateEditText.setOnClickListener(v -> birthDatePicker.show(getSupportFragmentManager(), null));
        birthDatePicker.addOnPositiveButtonClickListener(selection -> {
            selectedBirthDateTimestamp = selection;
            setBirthDateEditTextData();
        });
        loginTextView.setOnClickListener(v -> getOnBackPressedDispatcher().onBackPressed());
        submitButton.setOnClickListener(v -> {
            // Validation
            String emailAddress = String.valueOf(emailAddressEditText.getText()).trim();
            if (!Pattern.compile("^(.+)@(\\S+)$").matcher(emailAddress).matches()) {
                emailAddressEditTextLayout.setError("Email address is invalid.");
                return;
            } else if (emailAddressEditTextLayout.getError() != null) {
                emailAddressEditTextLayout.setError(null);
            }

            String password = String.valueOf(passwordEditText.getText());
            if (password.length() < 8) {
                passwordEditTextLayout.setError("Password must be at least 8 characters long.");
                return;
            } else if (passwordEditTextLayout.getError() != null) {
                passwordEditTextLayout.setError(null);
            }

            String fullName = String.valueOf(fullNameEditText.getText()).trim();
            if (fullName.length() < 2) {
                fullNameEditTextLayout.setError("Full name must be at least 2 characters long.");
                return;
            } else if (fullNameEditTextLayout.getError() != null) {
                fullNameEditTextLayout.setError(null);
            }

            int height;
            try {
                height = Integer.parseInt(String.valueOf(heightEditText.getText()).trim());
            } catch (NumberFormatException exception) {
                heightEditTextLayout.setError("Height must be an integer.");
                return;
            }
            if (height < 1) {
                heightEditTextLayout.setError("Height must be greater than 0.");
                return;
            } else if (heightEditTextLayout.getError() != null) {
                heightEditTextLayout.setError(null);
            }

            int weight;
            try {
                weight = Integer.parseInt(String.valueOf(weightEditText.getText()).trim());
            } catch (NumberFormatException exception) {
                weightEditTextLayout.setError("Weight must be an integer.");
                return;
            }
            if (weight < 1) {
                weightEditTextLayout.setError("Weight must be greater than 0.");
                return;
            } else if (weightEditTextLayout.getError() != null) {
                weightEditTextLayout.setError(null);
            }

            String phoneNumber = String.valueOf(phoneNumberEditText.getText()).trim();
            if (phoneNumber.isEmpty()) {
                phoneNumberEditTextLayout.setError("Phone number must be at least 1 character long.");
                return;
            } else if (phoneNumberEditTextLayout.getError() != null) {
                phoneNumberEditTextLayout.setError(null);
            }

            ActivityFactor selectedActivityFactor;
            switch (String.valueOf(activityFactorAutocompleteTextView.getText())) {
                case "Lightly active":
                    selectedActivityFactor = ActivityFactor.LIGHTLY_ACTIVE;
                    break;
                case "Moderately active":
                    selectedActivityFactor = ActivityFactor.MODERATELY_ACTIVE;
                    break;
                case "Very active":
                    selectedActivityFactor = ActivityFactor.VERY_ACTIVE;
                    break;
                case "Extra active":
                    selectedActivityFactor = ActivityFactor.EXTRA_ACTIVE;
                    break;
                default:
                    selectedActivityFactor = ActivityFactor.SEDENTARY;
                    break;
            }

            int genderRadioGroupSelectedId = genderRadioGroup.getCheckedRadioButtonId();
            MaterialRadioButton selectedGenderRadioButton = findViewById(genderRadioGroupSelectedId);
            Gender selectedGender =
                    String.valueOf(selectedGenderRadioButton.getText()).equals("Female") ? Gender.FEMALE : Gender.MALE;

            int userTypeRadioGroupSelectedId = userTypeRadioGroup.getCheckedRadioButtonId();
            MaterialRadioButton selectedUserTypeRadioButton = findViewById(userTypeRadioGroupSelectedId);
            UserType selectedUserType = String.valueOf(selectedUserTypeRadioButton.getText()).equals("Admin") ?
                    UserType.ADMIN : UserType.REGULAR;

            User newUser = new User(
                    selectedActivityFactor,
                    String.valueOf(selectedBirthDateTimestamp),
                    selectedGender,
                    height,
                    phoneNumber,
                    selectedUserType,
                    weight
            );
            firebaseAuth.createUserWithEmailAndPassword(emailAddress, password)
                    .addOnCompleteListener(signInTask -> {
                        if (signInTask.isSuccessful()) {
                            FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();

                            if (firebaseUser == null) {
                                return;
                            }

                            firebaseUser.sendEmailVerification().addOnCompleteListener(sendEmailVerificationTask -> {
                                if (sendEmailVerificationTask.isSuccessful()) {
                                    UserProfileChangeRequest profileChangeRequest = new UserProfileChangeRequest.Builder()
                                            .setDisplayName(fullName)
                                            .build();

                                    databaseReference.child("users")
                                            .child(firebaseUser.getUid())
                                            .setValue(newUser);
                                    firebaseUser.updateProfile(profileChangeRequest)
                                            .addOnCompleteListener(updateFirebaseUserProfileTask -> {
                                                if (updateFirebaseUserProfileTask.isSuccessful()) {
                                                    firebaseAuth.signOut();
                                                    Toast.makeText(
                                                            RegisterActivity.this,
                                                            "Please verify your email address",
                                                            Toast.LENGTH_SHORT
                                                    ).show();
                                                    getOnBackPressedDispatcher().onBackPressed();
                                                } else if (updateFirebaseUserProfileTask.getException() != null) {
                                                    emailAddressEditTextLayout.setError(updateFirebaseUserProfileTask
                                                            .getException()
                                                            .getLocalizedMessage());
                                                }
                                            });
                                } else if (sendEmailVerificationTask.getException() != null) {
                                    emailAddressEditTextLayout.setError(sendEmailVerificationTask
                                            .getException()
                                            .getLocalizedMessage());
                                }
                            });
                        } else if (signInTask.getException() != null) {
                            emailAddressEditTextLayout.setError(signInTask.getException().getLocalizedMessage());
                        }
                    })
                    .addOnFailureListener(exception -> {
                        String exceptionMessage = exception.getMessage();

                        if (exceptionMessage != null) {
                            emailAddressEditTextLayout.setError(exceptionMessage);
                        }
                    });
        });
    }

    private void setVariables() {
        activityFactorAutocompleteTextView = findViewById(R.id.registerActivityFactorAutoCompleteTextView);
        birthDatePicker = MaterialDatePicker.Builder
                .datePicker()
                .setInputMode(MaterialDatePicker.INPUT_MODE_TEXT)
                .setTextInputFormat(new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()))
                .setTitleText("Select birth date")
                .build();
        birthDateEditText = findViewById(R.id.registerBirthDateEditText);
        emailAddressEditText = findViewById(R.id.registerEmailAddressEditText);
        emailAddressEditTextLayout = findViewById(R.id.registerEmailAddressEditTextLayout);
        fullNameEditText = findViewById(R.id.registerFullNameEditText);
        fullNameEditTextLayout = findViewById(R.id.registerFullNameEditTextLayout);
        genderRadioGroup = findViewById(R.id.registerGenderRadioGroup);
        heightEditText = findViewById(R.id.registerHeightEditText);
        heightEditTextLayout = findViewById(R.id.registerHeightEditTextLayout);
        loginTextView = findViewById(R.id.registerLoginTextView);
        passwordEditText = findViewById(R.id.registerPasswordEditText);
        passwordEditTextLayout = findViewById(R.id.registerPasswordEditTextLayout);
        phoneNumberEditText = findViewById(R.id.registerPhoneNumberEditText);
        phoneNumberEditTextLayout = findViewById(R.id.registerPhoneNumberEditTextLayout);
        submitButton = findViewById(R.id.registerSubmitButton);
        userTypeRadioGroup = findViewById(R.id.registerTypeRadioGroup);
        weightEditText = findViewById(R.id.registerWeightEditText);
        weightEditTextLayout = findViewById(R.id.registerWeightEditTextLayout);
    }
}
