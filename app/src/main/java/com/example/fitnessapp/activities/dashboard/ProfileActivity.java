package com.example.fitnessapp.activities.dashboard;

import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.fitnessapp.R;
import com.example.fitnessapp.enums.ActivityFactor;
import com.example.fitnessapp.enums.Gender;
import com.example.fitnessapp.enums.UserType;
import com.example.fitnessapp.models.User;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.radiobutton.MaterialRadioButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ProfileActivity extends AppCompatActivity {
    private AutoCompleteTextView activityFactorAutocompleteTextView;
    private MaterialRadioButton adminRadioButton;
    private TextInputEditText birthDateEditText;
    private MaterialDatePicker<Long> birthDatePicker;
    private final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
    private MaterialRadioButton femaleRadioButton;
    private final FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    private TextInputEditText fullNameEditText;
    private TextInputLayout fullNameEditTextLayout;
    private RadioGroup genderRadioGroup;
    private TextView goBackTextView;
    private TextInputEditText heightEditText;
    private TextInputLayout heightEditTextLayout;
    private MaterialRadioButton maleRadioButton;
    private TextInputEditText phoneNumberEditText;
    private TextInputLayout phoneNumberEditTextLayout;
    private MaterialRadioButton regularUserRadioButton;
    private long selectedBirthDateTimestamp;
    private MaterialButton submitButton;
    private RadioGroup userTypeRadioGroup;
    private TextInputEditText weightEditText;
    private TextInputLayout weightEditTextLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        setVariables();
        setOnClickListeners();
        setActivityFactorsDropdown();
        displayUserData();
    }

    private void setActivityFactorsDropdown() {
        String[] activityFactors = getResources().getStringArray(R.array.activity_factors);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, R.layout.activity_factor_item, activityFactors);
        activityFactorAutocompleteTextView.setAdapter(arrayAdapter);
    }

    private void displayBirthDate(Long timestamp) {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        birthDateEditText.setText(dateFormat.format(new Date(timestamp)));
    }

    private void displayUserData() {
        String[] activityFactors = getResources().getStringArray(R.array.activity_factors);

        databaseReference.child("users")
                .child(firebaseUser.getUid())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        if (!snapshot.exists()) {
                            activityFactorAutocompleteTextView.setText(activityFactors[0], false);
                            return;
                        }

                        User userData = snapshot.getValue(User.class);

                        if (userData == null) {
                            activityFactorAutocompleteTextView.setText(activityFactors[0], false);
                            return;
                        }

                        switch (userData.getActivityFactor()) {
                            case EXTRA_ACTIVE:
                                activityFactorAutocompleteTextView.setText(activityFactors[4], false);
                                break;
                            case LIGHTLY_ACTIVE:
                                activityFactorAutocompleteTextView.setText(activityFactors[1], false);
                                break;
                            case MODERATELY_ACTIVE:
                                activityFactorAutocompleteTextView.setText(activityFactors[2], false);
                                break;
                            case SEDENTARY:
                                activityFactorAutocompleteTextView.setText(activityFactors[0], false);
                                break;
                            case VERY_ACTIVE:
                                activityFactorAutocompleteTextView.setText(activityFactors[3], false);
                                break;
                        }

                        Log.d("userData", userData.toString());
                        long userBirthDateTimestamp = Long.parseLong(userData.getBirthDate());
                        displayBirthDate(userBirthDateTimestamp);
                        selectedBirthDateTimestamp = userBirthDateTimestamp;

                        fullNameEditText.setText(firebaseUser.getDisplayName());
                        heightEditText.setText(String.valueOf(userData.getHeight()));
                        phoneNumberEditText.setText(userData.getPhoneNumber());
                        weightEditText.setText(String.valueOf(userData.getWeight()));

                        if (userData.getGender() == Gender.FEMALE) {
                            femaleRadioButton.setChecked(true);
                        } else {
                            maleRadioButton.setChecked(true);
                        }

                        if (userData.getType() == UserType.ADMIN) {
                            adminRadioButton.setChecked(true);
                        } else {
                            regularUserRadioButton.setChecked(true);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
    }

    private void setOnClickListeners() {
        birthDateEditText.setOnClickListener(v -> birthDatePicker.show(getSupportFragmentManager(), null));
        birthDatePicker.addOnPositiveButtonClickListener(selection -> {
            selectedBirthDateTimestamp = selection;
            displayBirthDate(selection);
        });
        goBackTextView.setOnClickListener(v -> getOnBackPressedDispatcher().onBackPressed());
        submitButton.setOnClickListener(v -> {
            // Validation
            String fullName = String.valueOf(fullNameEditText.getText()).trim();
            if (fullName.length() < 2) {
                fullNameEditTextLayout.setError("Full name must be at least 2 characters long.");
                return;
            } else if (fullNameEditTextLayout.getError() != null) {
                fullNameEditTextLayout.setError(null);
            }

            int height;
            try {
                height = Integer.parseInt(String.valueOf(heightEditText.getText()).trim());
            } catch (NumberFormatException exception) {
                heightEditTextLayout.setError("Height must be an integer.");
                return;
            }
            if (height < 1) {
                heightEditTextLayout.setError("Height must be greater than 0.");
                return;
            } else if (heightEditTextLayout.getError() != null) {
                heightEditTextLayout.setError(null);
            }

            int weight;
            try {
                weight = Integer.parseInt(String.valueOf(weightEditText.getText()).trim());
            } catch (NumberFormatException exception) {
                weightEditTextLayout.setError("Weight must be an integer.");
                return;
            }
            if (weight < 1) {
                weightEditTextLayout.setError("Weight must be greater than 0.");
                return;
            } else if (weightEditTextLayout.getError() != null) {
                weightEditTextLayout.setError(null);
            }

            String phoneNumber = String.valueOf(phoneNumberEditText.getText()).trim();
            if (phoneNumber.isEmpty()) {
                phoneNumberEditTextLayout.setError("Phone number must be at least 1 character long.");
                return;
            } else if (phoneNumberEditTextLayout.getError() != null) {
                phoneNumberEditTextLayout.setError(null);
            }

            ActivityFactor selectedActivityFactor;
            switch (String.valueOf(activityFactorAutocompleteTextView.getText())) {
                case "Lightly active":
                    selectedActivityFactor = ActivityFactor.LIGHTLY_ACTIVE;
                    break;
                case "Moderately active":
                    selectedActivityFactor = ActivityFactor.MODERATELY_ACTIVE;
                    break;
                case "Very active":
                    selectedActivityFactor = ActivityFactor.VERY_ACTIVE;
                    break;
                case "Extra active":
                    selectedActivityFactor = ActivityFactor.EXTRA_ACTIVE;
                    break;
                default:
                    selectedActivityFactor = ActivityFactor.SEDENTARY;
                    break;
            }

            int genderRadioGroupSelectedId = genderRadioGroup.getCheckedRadioButtonId();
            MaterialRadioButton selectedGenderRadioButton = findViewById(genderRadioGroupSelectedId);
            Gender selectedGender =
                    String.valueOf(selectedGenderRadioButton.getText()).equals("Female") ? Gender.FEMALE : Gender.MALE;

            int userTypeRadioGroupSelectedId = userTypeRadioGroup.getCheckedRadioButtonId();
            MaterialRadioButton selectedUserTypeRadioButton = findViewById(userTypeRadioGroupSelectedId);
            UserType selectedUserType = String.valueOf(selectedUserTypeRadioButton.getText()).equals("Admin") ?
                    UserType.ADMIN : UserType.REGULAR;

            User updatedUserData = new User(
                    selectedActivityFactor,
                    String.valueOf(selectedBirthDateTimestamp),
                    selectedGender,
                    height,
                    phoneNumber,
                    selectedUserType,
                    weight
            );

            if (!fullName.trim().equals(firebaseUser.getDisplayName())) {
                UserProfileChangeRequest profileChangeRequest = new UserProfileChangeRequest.Builder()
                        .setDisplayName(fullName)
                        .build();
                firebaseUser.updateProfile(profileChangeRequest)
                        .addOnCompleteListener(updateFirebaseUserProfileTask -> {
                            if (updateFirebaseUserProfileTask.isSuccessful()) {
                                Log.d("updatedDisplayName", "Successful");
                            } else if (updateFirebaseUserProfileTask.getException() != null) {
                                fullNameEditTextLayout.setError(updateFirebaseUserProfileTask
                                        .getException()
                                        .getLocalizedMessage());
                            }
                        });
            }

            databaseReference.child("users")
                    .child(firebaseUser.getUid())
                    .setValue(updatedUserData)
                    .addOnCompleteListener(task -> {
                        goBackTextView.callOnClick();
                        Toast.makeText(ProfileActivity.this, "Profile updated", Toast.LENGTH_SHORT).show();
                    })
                    .addOnFailureListener(exception -> {
                        if (exception.getLocalizedMessage() != null) {
                            Log.d("updatedUserDataException", exception.getLocalizedMessage());
                        }
                    });
        });
    }

    private void setVariables() {
        activityFactorAutocompleteTextView = findViewById(R.id.profileActivityFactorAutoCompleteTextView);
        adminRadioButton = findViewById(R.id.profileAdminRadioButton);
        birthDateEditText = findViewById(R.id.profileBirthDateEditText);
        birthDatePicker = MaterialDatePicker.Builder
                .datePicker()
                .setInputMode(MaterialDatePicker.INPUT_MODE_TEXT)
                .setTextInputFormat(new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()))
                .setTitleText("Select birth date")
                .build();
        femaleRadioButton = findViewById(R.id.profileFemaleRadioButton);
        fullNameEditText = findViewById(R.id.profileFullNameEditText);
        fullNameEditTextLayout = findViewById(R.id.profileFullNameEditTextLayout);
        genderRadioGroup = findViewById(R.id.profileGenderRadioGroup);
        goBackTextView = findViewById(R.id.profileGoBackTextView);
        heightEditText = findViewById(R.id.profileHeightEditText);
        heightEditTextLayout = findViewById(R.id.profileHeightEditTextLayout);
        maleRadioButton = findViewById(R.id.profileMaleRadioButton);
        phoneNumberEditText = findViewById(R.id.profilePhoneNumberEditText);
        phoneNumberEditTextLayout = findViewById(R.id.profilePhoneNumberEditTextLayout);
        regularUserRadioButton = findViewById(R.id.profileRegularRadioButton);
        submitButton = findViewById(R.id.profileSubmitButton);
        userTypeRadioGroup = findViewById(R.id.profileTypeRadioGroup);
        weightEditText = findViewById(R.id.profileWeightEditText);
        weightEditTextLayout = findViewById(R.id.profileWeightEditTextLayout);
    }
}
