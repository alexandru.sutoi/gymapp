package com.example.fitnessapp.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.fitnessapp.R;
import com.example.fitnessapp.activities.auth.LoginActivity;
import com.example.fitnessapp.activities.dashboard.DashboardActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class StartActivity extends AppCompatActivity {
    private final FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    private final SplashScreenLauncher launcher = new SplashScreenLauncher();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        launcher.start();
    }

    public class SplashScreenLauncher extends Thread {
        @Override
        public void run() {
            try {
                sleep(2000);
            } catch (InterruptedException exception) {
                exception.printStackTrace();
            } finally {
                Intent intent = new Intent(StartActivity.this,
                        firebaseUser != null ? DashboardActivity.class : LoginActivity.class);
                finishAffinity();
                startActivity(intent);
            }
        }
    }
}
