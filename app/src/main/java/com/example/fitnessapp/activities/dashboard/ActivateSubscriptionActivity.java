package com.example.fitnessapp.activities.dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.example.fitnessapp.R;
import com.example.fitnessapp.models.SubscriptionPlan;
import com.example.fitnessapp.models.SubscriptionPlanDetails;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.slider.Slider;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;
import java.util.Date;

public class ActivateSubscriptionActivity extends AppCompatActivity {
    private final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
    private final FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    private TextView goBackTextView;
    private CoordinatorLayout mainLayout;
    private MaterialButton submitButton;
    private TextView subscriptionDescriptionValueTextView;
    private int subscriptionDurationMonths = 1;
    private Slider subscriptionDurationSlider;
    private TextView subscriptionNameValueTextView;
    private TextView subscriptionPriceValueTextView;
    private SubscriptionPlan subscriptionPlan;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activate_subscription);
        setVariables();
        setSubscriptionPlan();
        setOnClickListeners();
        subscriptionDurationSlider.addOnChangeListener((slider, value, fromUser) -> {
            subscriptionDurationMonths = Math.round(value);
            subscriptionPriceValueTextView.setText(String.valueOf(Math.round(subscriptionPlan.getPrice() * value)));
        });
        displaySubscriptionPlanDetails();
    }

    private void displaySubscriptionPlanDetails() {
        subscriptionDescriptionValueTextView.setText(subscriptionPlan.getDescription());
        subscriptionNameValueTextView.setText(subscriptionPlan.getName());
        subscriptionPriceValueTextView.setText(String.valueOf(subscriptionPlan.getPrice()));
    }

    private void setOnClickListeners() {
        goBackTextView.setOnClickListener(v -> getOnBackPressedDispatcher().onBackPressed());
        submitButton.setOnClickListener(v -> {
            submitButton.setEnabled(false);
            Date startDate = new Date();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(startDate);
            calendar.add(Calendar.MONTH, subscriptionDurationMonths);
            Date endDate = calendar.getTime();

            databaseReference.child("subscriptions")
                    .child(firebaseUser.getUid())
                    .orderByChild("endDateTime")
                    .limitToLast(1)
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            if (!snapshot.exists()) {
                                databaseReference.child("subscriptions")
                                        .child(firebaseUser.getUid())
                                        .push()
                                        .setValue(new SubscriptionPlanDetails(
                                                String.valueOf(endDate.getTime()),
                                                String.valueOf(startDate.getTime()),
                                                subscriptionPlan.getId()
                                        ));

                                Snackbar snackbar = Snackbar.make(
                                                mainLayout,
                                                "Subscription added successfully",
                                                Snackbar.LENGTH_SHORT
                                        )
                                        .setBackgroundTint(getResources().getColor(R.color.gunmetal, getTheme()))
                                        .setTextColor(getResources().getColor(R.color.silver, getTheme()));

                                snackbar.addCallback(new Snackbar.Callback() {
                                    @Override
                                    public void onDismissed(Snackbar transientBottomBar, int event) {
                                        super.onDismissed(transientBottomBar, event);
                                        Intent intent = new Intent(
                                                ActivateSubscriptionActivity.this,
                                                DashboardActivity.class
                                        );
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                    }
                                });
                                snackbar.show();
                                return;
                            }

                            boolean isSubscriptionActive = false;
                            for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                                SubscriptionPlanDetails subscriptionPlanDetails =
                                        dataSnapshot.getValue(SubscriptionPlanDetails.class);
                                if (subscriptionPlanDetails != null) {
                                    Date subscriptionPlanEndDate =
                                            new Date(Long.parseLong(subscriptionPlanDetails.getEndDateTime()));

                                    if (subscriptionPlanEndDate.after(new Date())) {
                                        isSubscriptionActive = true;
                                    }
                                }
                            }

                            if (isSubscriptionActive) {
                                Snackbar snackbar = Snackbar.make(
                                                mainLayout,
                                                "You already have an active subscription",
                                                Snackbar.LENGTH_SHORT
                                        )
                                        .setBackgroundTint(getResources().getColor(R.color.gunmetal, getTheme()))
                                        .setTextColor(getResources().getColor(R.color.silver, getTheme()));

                                snackbar.addCallback(new Snackbar.Callback() {
                                    @Override
                                    public void onDismissed(Snackbar transientBottomBar, int event) {
                                        super.onDismissed(transientBottomBar, event);
                                        getOnBackPressedDispatcher().onBackPressed();
                                    }
                                });
                                snackbar.show();
                                return;
                            }

                            databaseReference.child("subscriptions")
                                    .child(firebaseUser.getUid())
                                    .push()
                                    .setValue(new SubscriptionPlanDetails(
                                            String.valueOf(endDate.getTime()),
                                            String.valueOf(startDate.getTime()),
                                            subscriptionPlan.getId()
                                    ));

                            Snackbar snackbar = Snackbar.make(
                                            mainLayout,
                                            "Subscription added successfully",
                                            Snackbar.LENGTH_SHORT
                                    )
                                    .setBackgroundTint(getResources().getColor(R.color.gunmetal, getTheme()))
                                    .setTextColor(getResources().getColor(R.color.silver, getTheme()));

                            snackbar.addCallback(new Snackbar.Callback() {
                                @Override
                                public void onDismissed(Snackbar transientBottomBar, int event) {
                                    super.onDismissed(transientBottomBar, event);
                                    Intent intent = new Intent(
                                            ActivateSubscriptionActivity.this,
                                            DashboardActivity.class
                                    );
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                }
                            });
                            snackbar.show();
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });
        });
    }

    private void setSubscriptionPlan() {
        Intent intent = getIntent();
        subscriptionPlan = (SubscriptionPlan) intent.getSerializableExtra("subscriptionPlan");
    }

    private void setVariables() {
        goBackTextView = findViewById(R.id.addUserSubscriptionGoBackTextView);
        mainLayout = findViewById(R.id.addUserSubscriptionMainLayout);
        submitButton = findViewById(R.id.addUserSubscriptionSubmitButton);
        subscriptionDescriptionValueTextView = findViewById(R.id.addUserSubscriptionDescriptionValueTextView);
        subscriptionDurationSlider = findViewById(R.id.addUserSubscriptionDurationSlider);
        subscriptionNameValueTextView = findViewById(R.id.addUserSubscriptionNameValueTextView);
        subscriptionPriceValueTextView = findViewById(R.id.addUserSubscriptionPriceValueTextView);
    }
}
