package com.example.fitnessapp.activities.dashboard.adminonly;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.example.fitnessapp.R;
import com.example.fitnessapp.models.SubscriptionPlan;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class AddSubscriptionPlanActivity extends AppCompatActivity {
    private final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
    private TextInputEditText descriptionEditText;
    private TextInputLayout descriptionEditTextLayout;
    private final FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    private TextView goBackTextView;
    private CoordinatorLayout mainLayout;
    private TextInputEditText nameEditText;
    private TextInputLayout nameEditTextLayout;
    private TextInputEditText priceEditText;
    private TextInputLayout priceEditTextLayout;
    private MaterialButton submitButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_subscription_plan);
        setVariables();
        setOnClickListeners();
    }

    private void setOnClickListeners() {
        goBackTextView.setOnClickListener(v -> getOnBackPressedDispatcher().onBackPressed());
        submitButton.setOnClickListener(v -> {
            // Validation
            String name = String.valueOf(nameEditText.getText()).trim();
            if (name.isEmpty()) {
                nameEditTextLayout.setError("Name must be at least one character long.");
                return;
            } else if (nameEditTextLayout.getError() != null) {
                nameEditTextLayout.setError(null);
            }

            String description = String.valueOf(descriptionEditText.getText()).trim();
            if (description.isEmpty()) {
                descriptionEditTextLayout.setError("Description must be at least one character long.");
                return;
            } else if (descriptionEditTextLayout.getError() != null) {
                descriptionEditTextLayout.setError(null);
            }

            long price;
            try {
                price = Long.parseLong(String.valueOf(priceEditText.getText()).trim());
            } catch (NumberFormatException exception) {
                priceEditTextLayout.setError("Price must be an integer.");
                return;
            }
            if (price < 1) {
                priceEditTextLayout.setError("Price must be greater than 0.");
                return;
            } else if (priceEditTextLayout.getError() != null) {
                priceEditTextLayout.setError(null);
            }

            SubscriptionPlan newSubscriptionPlan = new SubscriptionPlan(
                    description,
                    name,
                    price
            );

            databaseReference.child("subscriptionPlans").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if (!snapshot.exists()) {
                        databaseReference.child("subscriptionPlans")
                                .push()
                                .setValue(newSubscriptionPlan)
                                .addOnCompleteListener(task -> {
                                    if (task.isSuccessful()) {
                                        Toast.makeText(
                                                AddSubscriptionPlanActivity.this,
                                                "Subscription plan added successfully",
                                                Toast.LENGTH_SHORT
                                        ).show();
                                        goBackTextView.callOnClick();
                                    }
                                });
                        return;
                    }

                    boolean isNameUnique = true;
                    for (DataSnapshot subscriptionPlansSnapshot : snapshot.getChildren()) {
                        SubscriptionPlan plan = subscriptionPlansSnapshot.getValue(SubscriptionPlan.class);

                        if (plan != null && plan.getName().equals(newSubscriptionPlan.getName())) {
                            isNameUnique = false;
                            break;
                        }
                    }

                    if (!isNameUnique) {
                        Snackbar.make(
                                        mainLayout,
                                        "A plan with this name already exists",
                                        Snackbar.LENGTH_SHORT
                                )
                                .setBackgroundTint(getResources().getColor(R.color.gunmetal, getTheme()))
                                .setTextColor(getResources().getColor(R.color.silver, getTheme()))
                                .show();
                        return;
                    }

                    databaseReference.child("subscriptionPlans")
                            .push()
                            .setValue(newSubscriptionPlan)
                            .addOnCompleteListener(task -> {
                                if (task.isSuccessful()) {
                                    Toast.makeText(
                                            AddSubscriptionPlanActivity.this,
                                            "Subscription plan added successfully",
                                            Toast.LENGTH_SHORT
                                    ).show();
                                    goBackTextView.callOnClick();
                                }
                            });
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        });
    }

    private void setVariables() {
        descriptionEditText = findViewById(R.id.addSubscriptionPlanDescriptionEditText);
        descriptionEditTextLayout = findViewById(R.id.addSubscriptionPlanDescriptionEditTextLayout);
        goBackTextView = findViewById(R.id.addSubscriptionPlanGoBackTextView);
        mainLayout = findViewById(R.id.addSubscriptionPlanMainLayout);
        nameEditText = findViewById(R.id.addSubscriptionPlanNameEditText);
        nameEditTextLayout = findViewById(R.id.addSubscriptionPlanNameEditTextLayout);
        priceEditText = findViewById(R.id.addSubscriptionPlanPriceEditText);
        priceEditTextLayout = findViewById(R.id.addSubscriptionPlanPriceEditTextLayout);
        submitButton = findViewById(R.id.addSubscriptionPlanSubmitButton);
    }
}
