package com.example.fitnessapp.models;

import androidx.annotation.NonNull;

import com.google.firebase.database.Exclude;

public class SubscriptionPlanDetails {
    private String endDateTime;
    @Exclude
    private String id;
    private String startDateTime;
    private String subscriptionPlanId;

    public SubscriptionPlanDetails() {
        // Default constructor required for calls to DataSnapshot.getValue(SubscriptionPlanDetails.class)
    }

    public SubscriptionPlanDetails(String endDateTime, String startDateTime, String subscriptionPlanId) {
        this.endDateTime = endDateTime;
        this.startDateTime = startDateTime;
        this.subscriptionPlanId = subscriptionPlanId;
    }

    public String getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(String endDateTime) {
        this.endDateTime = endDateTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
    }

    public String getSubscriptionPlanId() {
        return subscriptionPlanId;
    }

    public void setSubscriptionPlanId(String subscriptionPlanId) {
        this.subscriptionPlanId = subscriptionPlanId;
    }

    @NonNull
    @Override
    public String toString() {
        return "SubscriptionPlanDetails{" +
                "endDateTime='" + endDateTime + '\'' +
                ", id='" + id + '\'' +
                ", startDateTime='" + startDateTime + '\'' +
                ", subscriptionPlanId='" + subscriptionPlanId + '\'' +
                '}';
    }
}
