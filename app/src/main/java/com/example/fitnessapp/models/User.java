package com.example.fitnessapp.models;

import androidx.annotation.NonNull;

import com.example.fitnessapp.enums.ActivityFactor;
import com.example.fitnessapp.enums.Gender;
import com.example.fitnessapp.enums.UserType;
import com.google.firebase.database.Exclude;

public class User {
    private ActivityFactor activityFactor;
    private String birthDate;
    private Gender gender;
    private Integer height;
    @Exclude
    private String id;
    private String phoneNumber;
    private UserType type;
    private Integer weight;

    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public User(
            ActivityFactor activityFactor,
            String birthDate,
            Gender gender,
            Integer height,
            String phoneNumber,
            UserType type,
            Integer weight
    ) {
        this.activityFactor = activityFactor;
        this.birthDate = birthDate;
        this.gender = gender;
        this.height = height;
        this.phoneNumber = phoneNumber;
        this.type = type;
        this.weight = weight;
    }

    public ActivityFactor getActivityFactor() {
        return activityFactor;
    }

    public void setActivityFactor(ActivityFactor activityFactor) {
        this.activityFactor = activityFactor;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public UserType getType() {
        return type;
    }

    public void setType(UserType type) {
        this.type = type;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    @NonNull
    @Override
    public String toString() {
        return "User{" +
                "activityFactor=" + activityFactor +
                ", birthDate='" + birthDate + '\'' +
                ", gender=" + gender +
                ", height=" + height +
                ", id='" + id + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", type=" + type +
                ", weight=" + weight +
                '}';
    }
}
