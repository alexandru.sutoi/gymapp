package com.example.fitnessapp.models;

import androidx.annotation.NonNull;

import com.google.firebase.database.Exclude;

import java.io.Serializable;

public class SubscriptionPlan implements Serializable {
    private String description;
    @Exclude
    private String id;
    private String name;
    private Long price;

    public SubscriptionPlan() {
        // Default constructor required for calls to DataSnapshot.getValue(SubscriptionPlan.class)
    }

    public SubscriptionPlan(String description, String name, Long price) {
        this.description = description;
        this.name = name;
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    @NonNull
    @Override
    public String toString() {
        return "SubscriptionPlan{" +
                "description='" + description + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
