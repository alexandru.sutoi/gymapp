package com.example.fitnessapp.models;

import androidx.annotation.NonNull;

public class GymEntry {
    private String endDateTime;
    private String startDateTime;

    public GymEntry() {
        // Default constructor required for calls to DataSnapshot.getValue(GymEntry.class)
    }

    public GymEntry(String endDateTime, String startDateTime) {
        this.endDateTime = endDateTime;
        this.startDateTime = startDateTime;
    }

    public String getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(String endDateTime) {
        this.endDateTime = endDateTime;
    }

    public String getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
    }

    @NonNull
    @Override
    public String toString() {
        return "GymEntry{" +
                "endDateTime='" + endDateTime + '\'' +
                ", startDateTime='" + startDateTime + '\'' +
                '}';
    }
}
