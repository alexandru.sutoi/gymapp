# GymApp

## Descriere
Aplicația mobilă a fost dezvoltată în scopul gestionării unei săli de fitness și a propriei nutriții oferind o soluție simplă și eficientă pentru optimizarea proceselor 
administrative și a experienței utilizatorilor. Procedurile și metodele implementate în aplicație includ funcționalități precum gestionarea abonamentelor, monitorizarea 
nutriției și urmărirea progresului individual.

### Clonare repository
- git clone https://gitlab.upt.ro/alexandru.sutoi/gymapp.git

### Tool-uri necesare:
- Android Studio

### Instalare Android Studio
1) Accesăm link-ul https://developer.android.com/studio și descărcăm ultima versiune
2) La instalare, căsuțele cu Android Studio + Android Virtual Device trebuie să fie bifate

### Compilare proiect
1) Importăm proiectul GymApp
2) Așteptăm până Gradle-ul construiește modelul. Se poate observa progresul în bara din dreapta-jos
3) Selectăm tab-ul Device Manager din panoul din dreapta
4) Creăm un nou Virtual Device
4) Verificăm să fie selectată opțiunea de Phone și selectăm Medium Phone
5) Se va descărca imaginea Nougat API Level 24, în caz că aceasta nu există
6) Rulăm aplicația din butonul de play din bara orizontală din dreapta-sus, sau prin apăsarea tastei Shift+F10
7) Așteptăm ca Gradle-ul să ruleze build-ul, iar apoi aplicația va porni


